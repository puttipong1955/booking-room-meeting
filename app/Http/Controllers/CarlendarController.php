<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Booking;
use Carbon\Carbon;
use DB;
use App\User;

class CarlendarController extends Controller
{
    public function sroomData()
    {
        $dateNow = Carbon::today()->format('Y-m-d');
        $data = Booking::where('date', $dateNow)->where('room', 's')->get();

        return view('s-room')->with('data', $data);
    }

    public function lroomData()
    {
        $dateNow = Carbon::today()->format('Y-m-d');
        $data = Booking::where('date', $dateNow)->where('room', 'l')->get();

        return view('l-room')->with('data', $data);
    }

    public function lroomDate($date)
    {
        $lroomDate = Booking::where('date', $date)->where('room', 'l')->get();

        return $lroomDate;
    }

    public function sroomDate($date)
    {
        $sroomDate = Booking::where('date', $date)->where('room', 's')->get();

        return $sroomDate;
    }

    public function getEvent($id)
    {
        $event = Booking::where('id', $id)->first();
        return $event;
    }

    public function addEvent(Request $request)
    {

        $request->validate([
            'subject' => 'required',
            'date' => 'required',
            'start' => 'required',
            'end' => 'required',
            'user_id' => 'required',
            'room' => 'required'
        ]);

        // Check Data
        $date = $request->input('date');
        $start = $request->input('start');
        $end = $request->input('end');

        //Convert Date
        $dateSplit = explode("-",$date);
        $dateConvert = $dateSplit[2].'-'.$dateSplit[1].'-'.$dateSplit[0];

            $i = $start;
            $result = '';
            while ($i < $end) {
                $result .= "$i,";
                $i += 0.5;
            }

            $book = new Booking;
            $book->user_id = $request->input('user_id');
            $book->room = $request->input('room');
            $book->subject = $request->input('subject');
            $book->date = $dateConvert;
            $book->start = $request->input('start');
            $book->end = $request->input('end');
            $book->event = $result;
            $book->save();

            if ($request->input('room') == 'l' && $request->input('admin') == '') {
                return redirect()->back();
            } else if ($request->input('room') == 'l' && $request->input('admin') == '1') {
                return redirect('/admin/l-room')->with('success', 'Data Saved');
            } else if ($request->input('room') == 's' && $request->input('admin') == '') {
                return redirect()->back();
            } else if ($request->input('room') == 's' && $request->input('admin') == '1') {
                return redirect('/admin/s-room')->with('success', 'Data Saved');
            }
    }

    public function delEvent($room, $id, $admin)
    {
        $book = Booking::findorfail($id);
        $book->delete();

        if ($room == 's' && $admin == '0') {
            return Redirect::to('/s-room');
        } else if ($room == 'l' && $admin == '0') {
            return Redirect::to('/l-room');
        } else if ($room == 's' && $admin == '1') {
            return Redirect::to('/admin/s-room');
        } else if ($room == 'l' && $admin == '1') {
            return Redirect::to('/admin/l-room');
        }
        // return ('Del data ');
    }

    public function testPost(Request $request)
    {
        return "test";
    }

    public function getUser($id){
        $user = User::where('id', $id)->first();
        return $user;
    }
}
