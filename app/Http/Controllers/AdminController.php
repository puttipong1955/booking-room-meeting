<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Booking;
use App\User;
use DB;


class AdminController extends Controller
{
    
   public function __construct()
   {
       $this->middleware('auth');
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */

    public function index(){

        $dateNow = Carbon::today()->format('Y-m-d');
        $sroomData = Booking::where('date', $dateNow)->where('room', 's')->get();
        $lroomData = Booking::where('date', $dateNow)->where('room', 'l')->get();
        $users = User::limit(10)->get();

        return view('/admin/index')
        ->with('sroomData', $sroomData)
        ->with('lroomData', $lroomData)
        ->with('users', $users);
    }

    public function getSroom(){
        $sroomDate = Booking::where('room', 's')->select('date')->groupby('date')->orderBy('date', 'DESC')->paginate(10);
        $sroomEvent = Booking::where('room', 's')->orderBy('start', 'asc')->get();
        $usersAll = User::select('id', 'name')->get();
        
        //return $sroomDate;
        return view('/admin/s-room')
            ->with('sroomDate', $sroomDate)
            ->with('sroomEvent', $sroomEvent)
            ->with('usersAll', $usersAll);
    }

    public function getLroom(){
        $lroomDate = Booking::where('room', 'l')->select('date')->groupby('date')->orderBy('date', 'DESC')->paginate(10);
        $lroomEvent = Booking::where('room', 'l')->orderBy('start', 'asc')->get();
        $usersAll = User::select('id','name')->get();
        
        return view('/admin/l-room')
            ->with('lroomDate', $lroomDate)
            ->with('lroomEvent', $lroomEvent)
            ->with('usersAll', $usersAll);
    }

    public function getUsers(){
        $users = User::paginate(15);
        return view('/admin/users')->with('users', $users);
    }

    public function getUser($id){
        $user = User::where('id', $id)->first();
        return $user;
    }

    public function register(Request $request)
    {
        $request->validate([
            'email'=>'required',
            'username'=>'required',
            'password'=>'required',
            'password2'=>'required']);

        // print_r($request);

        $user = new User;
        $user->email = $request->input('email');
        $user->name = $request->input('username');
        $user->password = bcrypt(request('password'));
        $user->save();

        return redirect('/admin/users')->with('success', 'Data Saved');
    }

    public function editUser(Request $request)
    {
        $request->validate([
            'email'=>'required',
            'username'=>'required',
            'password'=>'required']);

            $id = $request->input('id');
            $email = $request->input('email');
            $username = $request->input('username');
            $password = bcrypt(request('password'));

            DB::table('users')
            ->where('id',$id)
            ->update([
                'email'=>$email,
                'name'=>$username,
                'password'=>$password
                ]);

        return redirect('/admin/users')->with('success', 'Data Saved');
    }

    public function delUser($id, $admin)
    {
        $user = User::findorfail($id);
        $user->delete();

        if($admin == '0'){
            return Redirect::to('/'); 
        }else if($admin == '1'){
            return Redirect::to('/admin/users');
        }
    }
    
    public function getEventBetween($room, $from, $to){
        $eventBetween = Booking::where('room', $room)->whereBetween('date', [$from, $to])->orderByRaw('date ASC')->orderByRaw('start ASC')->paginate(20);
        return $eventBetween;
    }
}
