<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;
use App\Booking;

class CheckData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $request;
    }
}
