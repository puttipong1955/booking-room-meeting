$(function() {
    $( document ).ready(function() {
        var profile = $('#profile-name').val();
        var pathname = $(location).attr('href');
        var login = pathname.search( 'login' );
        var id = $('#profile-name').val();
        
        if(id == undefined){
          $('#addModal').modal('show');
          $('#add-user').css("display", "none");
          $('#login-user').css("display", "block");
        }

        if(login > 0){
          $('#addModal').modal('show');
          $('#add-user').css("display", "none");
          $('#login-user').css("display", "block");
        }

        if(profile == 'admin@web'){
          var current_url = document.URL;
          window.location.href = current_url + "admin";
        }else if(profile){
          $('#add-user').css("display", "none");
          $('#login-user').css("display", "none");
          $('#addModal').css('top', '-80px');
          $('#addModal').css('height', '150vh');
        }else{
          $('#profile-user').css("display", "none");
        }
      });

      $(document).on('click','.loginLabel',function(event){
          $('#add-user').css("display", "none");
          $('#login-user').css("display", "block");
      });

      $(document).on('click','.create-profile',function(event){
          $('#add-user').css("display", "block");
          $('#login-user').css("display", "none");
          $('#addModal').css('top', '-80px');
          $('#addModal').css('height', '150vh');
      });

      $(document).on('click','.delLabel',function(event){
        var id_user = $(this).attr("id");
        $("#alert-modal").modal('show');
        $(".alert-content").css('height', '200px')
        $(".alert-content").html('<p>You you sure?</p><div style="padding-bottom: 30px; height: 100px"><a href="/meeting/del/user/'+ id_user +'/0"><button class="btnYes">Yes</button></a><button class="btnNo" data-dismiss="modal">No</button></div>');
        $("#addModal").modal('hide');
      });

      $(document).on('submit','.profile-user',function(e){

        e.preventDefault();
        
          var pass1 =  $('#password').val();
          var pass2 =  $('#password-confirm').val();
          var form_data = $(this);
          var route = $(".profile-user").data("route");
          var username = $('#username').val();

          function hide_modal(){
            $("#alert-modal").modal('hide');
            $(".modal-backdrop").remove();
        }

        function hide_main_modal(){
            $("#addModal").modal('hide');
            $(".modal-backdrop").remove();
        }

        if(pass1 == pass2){
            $.ajax({
                type: "POST",
                url: route,
                data: form_data.serialize(),
                success: function( msg ) {
                    $('#user-name').html(username);
                    hide_main_modal();
                    $(".alert-content").html('<i class="fas fa-check-circle" style="position: absolute; right: 80px; top: 68px;"></i><p>Successfully</p>')
                    $('#alert-modal').modal('show');
                    window.setTimeout(hide_modal, 1500);

                },
                error: function(httpObj, textStatus) { 
                    alert('error');
                }
            });
        }else{
            alert('not pass');
        }
      });
})
