$(function() {
    $( document ).ready(function() {
        $('#datepicker').datepicker({
            icons: {
                rightIcon: ''
            },
            format: 'dd-mm-yyyy'
        });
        $('#datepicker2').datepicker({
            icons: {
                rightIcon: ''
            },
            format: 'dd-mm-yyyy'
        });

        var tour_id = $('.date-head').val();
        var username= $('.date-head').attr('name');
        var room = $('#room').val();
        var current_url = document.URL;
          $('#9').html('');
          $('#95').html('');
          $('#10').html('');
          $('#105').html('');
          $('#11').html('');
          $('#115').html('');
          $('#12').html('');
          $('#125').html('');
          $('#13').html('');
          $('#135').html('');
          $('#14').html('');
          $('#145').html('');
          $('#15').html('');
          $('#155').html('');
          $('#16').html('');
          $('#165').html('');
          $('#17').html('');
          $('#175').html('');
          $('#18').html('');
          $('#185').html('');
          $('#19').html('');
          $('#195').html('');
          $('#20').html('');
          $('#205').html('');
          $('#21').html('');
          $('#215').html('');
          $('#22').html('');
          $('#225').html('');
          $('#23').html('');
          $('#235').html('');
          $('#24').html('');
          $('#245').html('');
        
        //-- Convert Date
        var dateBookArr = tour_id.split('-');
        var dateConvert = dateBookArr[2].concat('-' + dateBookArr[1] + '-').concat(dateBookArr[0]);
        $.get(current_url + '/' + dateConvert, function (data) {
            //success data
            console.log(data);
            $.each(data, function(index, value) {
                switch (value.start) {
                    case 8:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#9').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#9').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#9').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#9').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#9').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#9').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#9').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#9').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#9').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#9').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#9').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#9').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#9').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#9').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#9').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#9').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#9').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#9').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#9').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#9').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#9').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#9').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#9').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#9').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#9').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#9').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#9').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#9').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#9').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#9').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#9').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#9').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 8.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#95').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#95').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#95').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#95').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#95').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#95').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#95').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#95').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#95').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#95').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#95').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#95').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#95').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#95').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#95').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#95').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#95').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#95').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#95').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#95').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#95').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#95').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#95').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#95').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#95').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#95').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#95').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#95').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#95').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#95').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#95').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#95').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#10').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#10').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#10').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#10').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#10').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#10').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#10').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#10').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#10').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#10').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#10').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#10').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#10').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#10').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#10').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#10').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#10').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#10').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#10').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#10').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#10').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#10').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#10').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#10').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#10').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#10').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#10').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#10').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#10').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#10').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#10').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#10').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9.5:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#105').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#105').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#105').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#105').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#105').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#105').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#105').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#105').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#105').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#105').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#105').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#105').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#105').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#105').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#105').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#105').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#105').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#105').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#105').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#105').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#105').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#105').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#105').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#105').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#105').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#105').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#105').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#105').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#105').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#105').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#105').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#105').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 10:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#11').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#11').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#11').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#11').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#11').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#11').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#11').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#11').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#11').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#11').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#11').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#11').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#11').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#11').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#11').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#11').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#11').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#11').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#11').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#11').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#11').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#11').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#11').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#11').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#11').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#11').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#11').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#11').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#11').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#11').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#11').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#11').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 10.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#115').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#115').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#115').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#115').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#115').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#115').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#115').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#115').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#115').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#115').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#115').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#115').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#115').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#115').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#115').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#115').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#115').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#115').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#115').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#115').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#115').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#115').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#115').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#115').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#115').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#115').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#115').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#115').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#115').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#115').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#115').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#115').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 11:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#12').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#12').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#12').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#12').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#12').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#12').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#12').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#12').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#12').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#12').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#12').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#12').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#12').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#12').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#12').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#12').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#12').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#12').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#12').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#12').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#12').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#12').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#12').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#12').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#12').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#12').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#12').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#12').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#12').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#12').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#12').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#12').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 11.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#125').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#125').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#125').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#125').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#125').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#125').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#125').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#125').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#125').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#125').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#125').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#125').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#125').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#125').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#125').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#125').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#125').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#125').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#125').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#125').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#125').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#125').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#125').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#125').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#125').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#125').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#125').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#125').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#125').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#125').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#125').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#125').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#13').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#13').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#13').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#13').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#13').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#13').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#13').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#13').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#13').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#13').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#13').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#13').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#13').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#13').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#13').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#13').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#13').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#13').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#13').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#13').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#13').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#13').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#13').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#13').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#13').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#13').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#13').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#13').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#13').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#13').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#13').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#13').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#135').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#135').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#135').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#135').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#135').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#135').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#135').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#135').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#135').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#135').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#135').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#135').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#135').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#135').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#135').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#135').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#135').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#135').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#135').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#135').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#135').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#135').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#135').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#135').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#135').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#135').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#135').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#135').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#135').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#135').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#135').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#135').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#14').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#14').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#14').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#14').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#14').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#14').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#14').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#14').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#14').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#14').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#14').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#14').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#14').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#14').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#14').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#14').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#14').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#14').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#14').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#14').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#14').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#14').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#14').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#14').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#14').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#14').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#14').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#14').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#14').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#14').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#14').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#14').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#145').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#145').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#145').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#145').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#145').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#145').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#145').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#145').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#145').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#145').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#145').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#145').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#145').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#145').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#145').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#145').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#145').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#145').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#145').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#145').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#145').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#145').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#145').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#145').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#145').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#145').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#145').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#145').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#145').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#145').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#145').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#145').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#15').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#15').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#15').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#15').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#15').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#15').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#15').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#15').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#15').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#15').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#15').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#15').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#15').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#15').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#15').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#15').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#15').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#15').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#15').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#15').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#15').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#15').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#15').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#15').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#15').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#15').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#15').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#15').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#15').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#15').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#15').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#15').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#155').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#155').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#155').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#155').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#155').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#155').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#155').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#155').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#155').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#155').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#155').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#155').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#155').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#155').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#155').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#155').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#155').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#155').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#155').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#155').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#155').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#155').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#155').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#155').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#155').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#155').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#155').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#155').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#155').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#155').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#155').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#155').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#16').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#16').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#16').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#16').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#16').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#16').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#16').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#16').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#16').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#16').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#16').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#16').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#16').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#16').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#16').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#16').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#16').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#16').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#16').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#16').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#16').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#16').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#16').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#16').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#16').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#16').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#16').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#16').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#16').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#16').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#16').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#16').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#165').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#165').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#165').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#165').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#165').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#165').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#165').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#165').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#165').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#165').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#165').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#165').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#165').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#165').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#165').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#165').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#165').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#165').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#165').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#165').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#165').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#165').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#165').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#165').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#165').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#165').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#165').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#165').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#165').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#165').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#165').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#165').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#17').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#17').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#17').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#17').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#17').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#17').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#17').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#17').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#17').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#17').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#17').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#17').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#17').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#17').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#17').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#17').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#17').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#17').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#17').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#17').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#17').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#17').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#17').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#17').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#17').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#17').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#17').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#17').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#17').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#17').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#17').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#17').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#175').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#175').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#175').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#175').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#175').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#175').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#175').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#175').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#175').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#175').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#175').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#175').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#175').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#175').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#175').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#175').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#175').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#175').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#175').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#175').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#175').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#175').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#175').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#175').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#175').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#175').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#175').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#175').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#175').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#175').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#175').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#175').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#18').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#18').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#18').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#18').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#18').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#18').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#18').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#18').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#18').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#18').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#18').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#18').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#18').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#18').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#18').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#18').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#18').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#18').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#18').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#18').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#18').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#18').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#18').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#18').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#18').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#18').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#18').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#18').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#18').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#18').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#18').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#18').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#185').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#185').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#185').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#185').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#185').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#185').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#185').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#185').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#185').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#185').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#185').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#185').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#185').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#185').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#185').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#185').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#185').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#185').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#185').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#185').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#185').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#185').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#185').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#185').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#185').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#185').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#185').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#185').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#185').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#185').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#185').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#185').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#19').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#19').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#19').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#19').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#19').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#19').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#19').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#19').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#19').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#19').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#19').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#19').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#19').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#19').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#19').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#19').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#19').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#19').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#19').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#19').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#19').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#19').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#19').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#19').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#19').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#19').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#19').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#19').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#19').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#19').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#19').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#19').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#195').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#195').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#195').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#195').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#195').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#195').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#195').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#195').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#195').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#195').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#195').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#195').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#195').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#195').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#195').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#195').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#195').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#195').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#195').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#195').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#195').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#195').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#195').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#195').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#195').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#195').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#195').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#195').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#195').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#195').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#195').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#195').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#20').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#20').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#20').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#20').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#20').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#20').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#20').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#20').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#20').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#20').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#20').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#20').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#20').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#20').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#20').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#20').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#20').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#20').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#20').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#20').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#20').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#20').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#20').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#20').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#20').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#20').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#20').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#20').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#20').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#20').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#20').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#20').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#205').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#205').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#205').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#205').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#205').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#205').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#205').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#205').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#205').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#205').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#205').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#205').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#205').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#205').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#205').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#205').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#205').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#205').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#205').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#205').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#205').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#205').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#205').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#205').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#205').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#205').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#205').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#205').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#205').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#205').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#205').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#205').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#21').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#21').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#21').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#21').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#21').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#21').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#21').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#21').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#21').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#21').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#21').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#21').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#21').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#21').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#21').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#21').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#21').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#21').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#21').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#21').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#21').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#21').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#21').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#21').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#21').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#21').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#21').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#21').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#21').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#21').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#21').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#21').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#215').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#215').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#215').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#215').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#215').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#215').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#215').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#215').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#215').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#215').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#215').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#215').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#215').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#215').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#215').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#215').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#215').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#215').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#215').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#215').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#215').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#215').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#215').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#215').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#215').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#215').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#215').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#215').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#215').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#215').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#215').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#215').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#22').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#22').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#22').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#22').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#22').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#22').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#22').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#22').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#22').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#22').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#22').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#22').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#22').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#22').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#22').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#22').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#22').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#22').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#22').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#22').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#22').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#22').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#22').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#22').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#22').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#22').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#22').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#22').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#22').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#22').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#22').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#22').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#225').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#225').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#225').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#225').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#225').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#225').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#225').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#225').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#225').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#225').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#225').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#225').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#225').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#225').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#225').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#225').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#225').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#225').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#225').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#225').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#225').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#225').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#225').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#225').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#225').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#225').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#225').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#225').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#225').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#225').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#225').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#225').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#23').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#23').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#23').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#23').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#23').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#23').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#23').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#23').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#23').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#23').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#23').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#23').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#23').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#23').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#23').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#23').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#23').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#23').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#23').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#23').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#23').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#23').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#23').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#23').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#23').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#23').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#23').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#23').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#23').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#23').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#23').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#23').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#235').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#235').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#235').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#235').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#235').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#235').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#235').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#235').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#235').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#235').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#235').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#235').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#235').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#235').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#235').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#235').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#235').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#235').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#235').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#235').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#235').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#235').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#235').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#235').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#235').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#235').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#235').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#235').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#235').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#235').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#235').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#235').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#24').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#24').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#24').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#24').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#24').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#24').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#24').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#24').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#24').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#24').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#24').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#24').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#24').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#24').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#24').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#24').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#24').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#24').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#24').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#24').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#24').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#24').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#24').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#24').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#24').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#24').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#24').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#24').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#24').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#24').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#24').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#24').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#245').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#245').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#245').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#245').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#245').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#245').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#245').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#245').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#245').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#245').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#245').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#245').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#245').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#245').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#245').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#245').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#245').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#245').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#245').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#245').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#245').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#245').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#245').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#245').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#245').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#245').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#245').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#245').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#245').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#245').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#245').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#245').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                }
            });
        }) ;
    });

    function showDetail(tour_id, session_id){
        var current_url = document.URL;
        var current_urlArr = current_url.split('/');
        var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];
        $.get(url+'/event'+'/'+tour_id, function (data) {
            //success data
            console.log(data);

            if(data.user_id == 1){
                $('#username').html('Admin');
            }else{
                //Get name user
                $.get(url + '/user/' + data.user_id, function (data) {
                    $('#username').html(data.name);
                });
            }

            var dateArr = data.date.split('-');
            var dateConvert = dateArr[2].concat('-' + dateArr[1] + '-').concat(dateArr[0]);
            
            $('#id').val(data.id);
            $('#subject').val(data.subject);
            $('#start').val(data.start);
            $('#start').attr('disabled', true);
            $('#end').val(data.end);
            $('#end').attr('disabled', true);
            $('.date-label').html(dateConvert).css('display', 'block');
            $('#exampleModal').modal('show');
            $('#btn').css("display", "none");
            if(session_id == data.user_id){
                $('.del-event').css("display", "block");
                $('.del-event').attr('id', data.id);  
            }else{
                $('.del-event').css("display", "none");
            }
        }); 
    }

    $(document).on('click','.book0-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id);  
        $('#subject').attr('readonly', true);
    });

    $(document).on('click','.book1',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id);    
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book1-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);        
      });

    $(document).on('click','.book2',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book2-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book3',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book3-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book4',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book4-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

    $(document).on('click','.book5-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

      $(document).on('click','.book6',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);        
      });

    $(document).on('click','.book6-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);        
      });

    $(document).on('click','.book7',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book7-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book8',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book8-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book9',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book9-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book10',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

    $(document).on('click','.book10-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

      $(document).on('click','.book11',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book11-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });  

    $(document).on('click','.book12',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

    $(document).on('click','.book12-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 

      $(document).on('click','.book13',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);        
      });

    $(document).on('click','.book13-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);        
      });

    $(document).on('click','.book14',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book14-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book15',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book15-5',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      });

    $(document).on('click','.book16',function(event){
        var tour_id = $(this).attr('id');
        var session_id = $(this).attr('name');
        showDetail(tour_id, session_id); 
        $('#subject').attr('readonly', true);
      }); 
      
    $(document).on('click','.clear',function(event){
          var dateHead = $('#datepicker').val();
          //var username= $(this).attr('name');
          $('#id').val('');
          $('#subject').val('');
          $('#start').val('');
          $('#end').val('');
          $('#datepicker2').val(dateHead);
          $('.del-event').css("display", "none");
          $('#btn').html("Add Book");
          $('#add-event').attr('action', "submit-event");
          $('#exampleModal').modal('show');
          $('#btn').css("display", "block");
          $('#subject').attr('readonly', false);
          $('#start').attr('disabled', false);
          $('#end').attr('disabled', false);
          $('.date-label').html('').css('display', 'none');
      }); 

      $(document).on('click','.del-event',function(event){
        var tour_id= $(this).attr('id');
        var username= $(this).attr('name');
        var date = $("#datepicker").val();
        var room = $("#room").val();
        var current_url = document.URL;
        var current_urlArr = current_url.split('/');
        var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];

        //-- Convert Date
        var dateBookArr = date.split('-');
        var dateConvert = dateBookArr[2].concat('-' + dateBookArr[1] + '-').concat(dateBookArr[0]);

        $.get(url +'/del-event/'+ room +'/' + tour_id + '/0') ;

        function hide_modal(){
            $("#alert-modal").modal('hide');
            $(".modal-backdrop").remove();
        }

        $(".alert-content").html('<i class="far fa-check-circle fa-2x" style="color: green; right: 115px; top: 68px;"></i><p>Successfull</p>')
        $("#alert-modal").modal('show');
        window.setTimeout(hide_modal, 2000)

      // Get Data Event
        var tour_id = dateConvert;
        var url2 = url + '/' + room +"-room";
        $('#9').html('');
        $('#95').html('');
        $('#10').html('');
        $('#105').html('');
        $('#11').html('');
        $('#115').html('');
        $('#12').html('');
        $('#125').html('');
        $('#13').html('');
        $('#135').html('');
        $('#14').html('');
        $('#145').html('');
        $('#15').html('');
        $('#155').html('');
        $('#16').html('');
        $('#165').html('');
        $('#17').html('');
        $('#175').html('');
        $('#18').html('');
        $('#185').html('');
        $('#19').html('');
        $('#195').html('');
        $('#20').html('');
        $('#205').html('');
        $('#21').html('');
        $('#215').html('');
        $('#22').html('');
        $('#225').html('');
        $('#23').html('');
        $('#235').html('');
        $('#24').html('');
        $('#245').html('');

        function RederLists() {
        $.get(url2 + '/' + tour_id, function (data) {
            //success data
            console.log(data);
            $.each(data, function(index, value) {
                switch (value.start) {
                    case 8:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#9').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#9').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#9').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#9').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#9').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#9').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#9').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#9').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#9').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#9').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#9').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#9').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#9').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#9').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#9').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#9').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#9').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#9').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#9').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#9').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#9').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#9').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#9').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#9').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#9').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#9').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#9').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#9').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#9').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#9').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#9').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#9').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 8.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#95').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#95').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#95').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#95').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#95').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#95').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#95').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#95').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#95').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#95').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#95').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#95').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#95').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#95').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#95').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#95').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#95').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#95').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#95').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#95').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#95').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#95').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#95').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#95').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#95').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#95').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#95').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#95').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#95').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#95').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#95').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#95').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#10').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#10').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#10').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#10').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#10').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#10').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#10').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#10').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#10').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#10').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#10').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#10').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#10').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#10').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#10').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#10').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#10').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#10').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#10').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#10').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#10').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#10').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#10').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#10').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#10').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#10').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#10').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#10').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#10').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#10').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#10').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#10').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9.5:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#105').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#105').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#105').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#105').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#105').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#105').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#105').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#105').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#105').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#105').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#105').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#105').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#105').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#105').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#105').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#105').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#105').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#105').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#105').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#105').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#105').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#105').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#105').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#105').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#105').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#105').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#105').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#105').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#105').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#105').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#105').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#105').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 10:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#11').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#11').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#11').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#11').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#11').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#11').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#11').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#11').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#11').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#11').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#11').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#11').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#11').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#11').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#11').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#11').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#11').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#11').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#11').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#11').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#11').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#11').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#11').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#11').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#11').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#11').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#11').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#11').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#11').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#11').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#11').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#11').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 10.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#115').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#115').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#115').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#115').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#115').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#115').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#115').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#115').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#115').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#115').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#115').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#115').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#115').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#115').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#115').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#115').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#115').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#115').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#115').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#115').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#115').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#115').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#115').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#115').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#115').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#115').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#115').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#115').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#115').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#115').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#115').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#115').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 11:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#12').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#12').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#12').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#12').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#12').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#12').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#12').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#12').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#12').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#12').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#12').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#12').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#12').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#12').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#12').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#12').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#12').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#12').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#12').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#12').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#12').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#12').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#12').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#12').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#12').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#12').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#12').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#12').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#12').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#12').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#12').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#12').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 11.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#125').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#125').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#125').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#125').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#125').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#125').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#125').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#125').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#125').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#125').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#125').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#125').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#125').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#125').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#125').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#125').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#125').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#125').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#125').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#125').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#125').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#125').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#125').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#125').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#125').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#125').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#125').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#125').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#125').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#125').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#125').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#125').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#13').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#13').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#13').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#13').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#13').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#13').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#13').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#13').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#13').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#13').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#13').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#13').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#13').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#13').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#13').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#13').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#13').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#13').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#13').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#13').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#13').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#13').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#13').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#13').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#13').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#13').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#13').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#13').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#13').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#13').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#13').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#13').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#135').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#135').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#135').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#135').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#135').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#135').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#135').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#135').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#135').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#135').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#135').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#135').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#135').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#135').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#135').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#135').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#135').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#135').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#135').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#135').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#135').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#135').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#135').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#135').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#135').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#135').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#135').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#135').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#135').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#135').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#135').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#135').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#14').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#14').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#14').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#14').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#14').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#14').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#14').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#14').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#14').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#14').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#14').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#14').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#14').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#14').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#14').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#14').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#14').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#14').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#14').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#14').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#14').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#14').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#14').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#14').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#14').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#14').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#14').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#14').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#14').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#14').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#14').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#14').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#145').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#145').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#145').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#145').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#145').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#145').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#145').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#145').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#145').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#145').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#145').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#145').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#145').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#145').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#145').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#145').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#145').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#145').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#145').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#145').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#145').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#145').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#145').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#145').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#145').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#145').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#145').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#145').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#145').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#145').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#145').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#145').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#15').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#15').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#15').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#15').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#15').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#15').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#15').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#15').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#15').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#15').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#15').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#15').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#15').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#15').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#15').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#15').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#15').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#15').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#15').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#15').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#15').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#15').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#15').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#15').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#15').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#15').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#15').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#15').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#15').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#15').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#15').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#15').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#155').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#155').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#155').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#155').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#155').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#155').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#155').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#155').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#155').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#155').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#155').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#155').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#155').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#155').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#155').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#155').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#155').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#155').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#155').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#155').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#155').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#155').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#155').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#155').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#155').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#155').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#155').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#155').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#155').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#155').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#155').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#155').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#16').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#16').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#16').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#16').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#16').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#16').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#16').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#16').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#16').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#16').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#16').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#16').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#16').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#16').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#16').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#16').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#16').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#16').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#16').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#16').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#16').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#16').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#16').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#16').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#16').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#16').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#16').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#16').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#16').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#16').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#16').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#16').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#165').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#165').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#165').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#165').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#165').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#165').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#165').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#165').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#165').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#165').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#165').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#165').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#165').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#165').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#165').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#165').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#165').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#165').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#165').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#165').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#165').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#165').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#165').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#165').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#165').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#165').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#165').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#165').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#165').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#165').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#165').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#165').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#17').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#17').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#17').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#17').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#17').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#17').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#17').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#17').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#17').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#17').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#17').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#17').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#17').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#17').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#17').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#17').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#17').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#17').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#17').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#17').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#17').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#17').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#17').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#17').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#17').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#17').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#17').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#17').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#17').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#17').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#17').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#17').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#175').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#175').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#175').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#175').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#175').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#175').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#175').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#175').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#175').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#175').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#175').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#175').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#175').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#175').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#175').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#175').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#175').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#175').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#175').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#175').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#175').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#175').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#175').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#175').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#175').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#175').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#175').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#175').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#175').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#175').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#175').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#175').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#18').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#18').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#18').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#18').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#18').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#18').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#18').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#18').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#18').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#18').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#18').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#18').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#18').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#18').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#18').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#18').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#18').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#18').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#18').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#18').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#18').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#18').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#18').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#18').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#18').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#18').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#18').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#18').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#18').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#18').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#18').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#18').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#185').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#185').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#185').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#185').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#185').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#185').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#185').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#185').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#185').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#185').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#185').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#185').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#185').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#185').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#185').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#185').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#185').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#185').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#185').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#185').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#185').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#185').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#185').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#185').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#185').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#185').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#185').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#185').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#185').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#185').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#185').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#185').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#19').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#19').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#19').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#19').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#19').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#19').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#19').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#19').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#19').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#19').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#19').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#19').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#19').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#19').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#19').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#19').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#19').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#19').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#19').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#19').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#19').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#19').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#19').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#19').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#19').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#19').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#19').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#19').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#19').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#19').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#19').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#19').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#195').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#195').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#195').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#195').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#195').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#195').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#195').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#195').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#195').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#195').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#195').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#195').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#195').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#195').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#195').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#195').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#195').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#195').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#195').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#195').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#195').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#195').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#195').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#195').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#195').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#195').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#195').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#195').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#195').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#195').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#195').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#195').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#20').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#20').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#20').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#20').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#20').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#20').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#20').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#20').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#20').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#20').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#20').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#20').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#20').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#20').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#20').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#20').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#20').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#20').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#20').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#20').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#20').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#20').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#20').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#20').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#20').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#20').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#20').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#20').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#20').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#20').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#20').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#20').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#205').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#205').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#205').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#205').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#205').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#205').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#205').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#205').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#205').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#205').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#205').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#205').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#205').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#205').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#205').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#205').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#205').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#205').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#205').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#205').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#205').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#205').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#205').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#205').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#205').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#205').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#205').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#205').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#205').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#205').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#205').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#205').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#21').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#21').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#21').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#21').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#21').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#21').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#21').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#21').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#21').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#21').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#21').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#21').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#21').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#21').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#21').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#21').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#21').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#21').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#21').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#21').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#21').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#21').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#21').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#21').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#21').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#21').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#21').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#21').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#21').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#21').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#21').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#21').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#215').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#215').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#215').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#215').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#215').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#215').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#215').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#215').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#215').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#215').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#215').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#215').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#215').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#215').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#215').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#215').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#215').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#215').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#215').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#215').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#215').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#215').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#215').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#215').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#215').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#215').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#215').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#215').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#215').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#215').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#215').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#215').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#22').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#22').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#22').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#22').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#22').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#22').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#22').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#22').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#22').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#22').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#22').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#22').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#22').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#22').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#22').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#22').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#22').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#22').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#22').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#22').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#22').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#22').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#22').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#22').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#22').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#22').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#22').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#22').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#22').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#22').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#22').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#22').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#225').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#225').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#225').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#225').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#225').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#225').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#225').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#225').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#225').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#225').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#225').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#225').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#225').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#225').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#225').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#225').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#225').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#225').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#225').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#225').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#225').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#225').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#225').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#225').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#225').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#225').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#225').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#225').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#225').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#225').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#225').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#225').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#23').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#23').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#23').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#23').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#23').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#23').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#23').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#23').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#23').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#23').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#23').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#23').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#23').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#23').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#23').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#23').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#23').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#23').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#23').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#23').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#23').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#23').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#23').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#23').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#23').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#23').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#23').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#23').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#23').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#23').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#23').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#23').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#235').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#235').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#235').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#235').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#235').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#235').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#235').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#235').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#235').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#235').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#235').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#235').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#235').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#235').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#235').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#235').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#235').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#235').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#235').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#235').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#235').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#235').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#235').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#235').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#235').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#235').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#235').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#235').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#235').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#235').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#235').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#235').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#24').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#24').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#24').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#24').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#24').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#24').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#24').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#24').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#24').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#24').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#24').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#24').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#24').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#24').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#24').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#24').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#24').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#24').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#24').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#24').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#24').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#24').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#24').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#24').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#24').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#24').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#24').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#24').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#24').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#24').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#24').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#24').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#245').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#245').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#245').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#245').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#245').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#245').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#245').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#245').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#245').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#245').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#245').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#245').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#245').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#245').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#245').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#245').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#245').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#245').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#245').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#245').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#245').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#245').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#245').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#245').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#245').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#245').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#245').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#245').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#245').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#245').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#245').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#245').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                }
            });
        });
        }

        setTimeout(RederLists, 1250);
      });

      $("#datepicker").change(function(){
          var tour_id= $(this).val();
          var username= $(this).attr('name');
          var room = $('#room').val();

          //-- Convert Date
            var dateBookArr = tour_id.split('-');
            var dateConvert = dateBookArr[2].concat('-' + dateBookArr[1] + '-').concat(dateBookArr[0]);

          var current_url = document.URL;
          $('#9').html('');
          $('#95').html('');
          $('#10').html('');
          $('#105').html('');
          $('#11').html('');
          $('#115').html('');
          $('#12').html('');
          $('#125').html('');
          $('#13').html('');
          $('#135').html('');
          $('#14').html('');
          $('#145').html('');
          $('#15').html('');
          $('#155').html('');
          $('#16').html('');
          $('#165').html('');
          $('#17').html('');
          $('#175').html('');
          $('#18').html('');
          $('#185').html('');
          $('#19').html('');
          $('#195').html('');
          $('#20').html('');
          $('#205').html('');
          $('#21').html('');
          $('#215').html('');
          $('#22').html('');
          $('#225').html('');
          $('#23').html('');
          $('#235').html('');
          $('#24').html('');
          $('#245').html('');
        $.get(current_url + '/' + dateConvert, function (data) {
            //success data
            console.log(data);
            $.each(data, function(index, value) {
                switch (value.start) {
                    case 8:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#9').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#9').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#9').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#9').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#9').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#9').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#9').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#9').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#9').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#9').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#9').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#9').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#9').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#9').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#9').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#9').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#9').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#9').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#9').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#9').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#9').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#9').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#9').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#9').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#9').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#9').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#9').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#9').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#9').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#9').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#9').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#9').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 8.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#95').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#95').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#95').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#95').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#95').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#95').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#95').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#95').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#95').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#95').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#95').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#95').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#95').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#95').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#95').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#95').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#95').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#95').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#95').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#95').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#95').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#95').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#95').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#95').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#95').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#95').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#95').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#95').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#95').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#95').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#95').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#95').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#10').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#10').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#10').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#10').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#10').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#10').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#10').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#10').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#10').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#10').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#10').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#10').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#10').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#10').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#10').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#10').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#10').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#10').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#10').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#10').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#10').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#10').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#10').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#10').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#10').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#10').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#10').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#10').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#10').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#10').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#10').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#10').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 9.5:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#105').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#105').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#105').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#105').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#105').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#105').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#105').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#105').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#105').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#105').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#105').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#105').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#105').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#105').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#105').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#105').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#105').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#105').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#105').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#105').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#105').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#105').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#105').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#105').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#105').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#105').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#105').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#105').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#105').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#105').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#105').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#105').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 10:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#11').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#11').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#11').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#11').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#11').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#11').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#11').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#11').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#11').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#11').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#11').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#11').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#11').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#11').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#11').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#11').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#11').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#11').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#11').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#11').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#11').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#11').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#11').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#11').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#11').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#11').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#11').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#11').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#11').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#11').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#11').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#11').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 10.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#115').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#115').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#115').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#115').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#115').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#115').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#115').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#115').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#115').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#115').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#115').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#115').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#115').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#115').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#115').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#115').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#115').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#115').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#115').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#115').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#115').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#115').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#115').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#115').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#115').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#115').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#115').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#115').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#115').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#115').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#115').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#115').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                    }
                    break;
                    case 11:
                        switch (value.end - value.start) {
                            case 0.5:
                                $('#12').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1:
                                $('#12').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 1.5:
                                $('#12').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2:
                                $('#12').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 2.5:
                                $('#12').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3:
                                $('#12').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 3.5:
                                $('#12').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4:
                                $('#12').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 4.5:
                                $('#12').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5:
                                $('#12').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                            case 5.5:
                                $('#12').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                break;
                        case 6:
                            $('#12').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#12').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#12').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#12').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#12').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#12').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#12').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#12').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#12').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#12').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#12').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#12').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#12').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#12').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#12').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#12').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#12').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#12').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#12').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#12').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#12').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 11.5:
                    switch (value.end - value.start) {
                        case 0.5:
                            $('#125').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#125').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#125').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#125').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#125').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#125').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#125').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#125').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#125').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#125').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#125').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#125').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#125').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#125').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#125').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#125').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#125').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#125').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#125').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#125').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#125').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#125').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#125').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#125').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#125').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#125').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#125').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#125').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#125').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#125').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#125').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#125').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#13').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#13').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#13').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#13').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#13').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#13').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#13').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#13').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#13').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#13').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#13').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#13').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#13').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#13').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#13').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#13').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#13').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#13').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#13').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#13').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#13').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#13').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#13').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#13').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#13').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#13').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#13').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#13').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#13').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#13').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#13').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#13').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 12.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#135').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#135').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#135').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#135').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#135').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#135').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#135').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#135').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#135').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#135').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#135').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#135').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#135').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#135').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#135').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#135').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#135').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#135').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#135').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#135').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#135').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#135').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#135').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#135').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#135').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#135').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#135').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#135').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#135').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#135').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#135').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#135').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#14').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#14').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#14').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#14').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#14').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#14').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#14').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#14').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#14').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#14').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#14').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#14').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#14').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#14').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#14').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#14').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#14').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#14').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#14').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#14').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#14').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#14').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#14').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#14').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#14').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#14').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#14').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#14').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#14').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#14').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#14').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#14').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 13.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#145').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#145').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#145').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#145').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#145').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#145').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#145').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#145').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#145').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#145').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#145').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#145').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#145').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#145').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#145').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#145').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#145').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#145').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#145').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#145').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#145').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#145').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#145').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#145').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#145').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#145').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#145').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#145').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#145').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#145').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#145').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#145').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#15').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#15').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#15').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#15').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#15').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#15').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#15').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#15').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#15').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#15').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#15').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#15').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#15').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#15').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#15').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#15').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#15').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#15').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#15').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#15').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#15').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#15').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#15').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#15').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#15').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#15').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#15').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#15').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#15').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#15').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#15').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#15').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 14.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#155').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#155').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#155').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#155').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#155').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#155').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#155').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#155').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#155').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#155').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#155').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#155').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#155').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#155').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#155').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#155').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#155').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#155').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#155').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#155').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#155').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#155').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#155').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#155').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#155').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#155').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#155').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#155').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#155').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#155').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#155').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#155').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#16').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#16').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#16').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#16').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#16').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#16').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#16').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#16').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#16').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#16').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#16').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#16').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#16').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#16').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#16').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#16').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#16').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#16').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#16').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#16').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#16').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#16').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#16').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#16').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#16').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#16').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#16').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#16').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#16').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#16').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#16').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#16').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 15.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#165').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#165').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#165').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#165').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#165').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#165').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#165').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#165').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#165').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#165').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#165').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#165').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#165').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#165').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#165').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#165').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#165').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#165').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#165').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#165').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#165').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#165').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#165').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#165').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#165').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#165').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#165').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#165').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#165').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#165').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#165').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#165').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#17').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#17').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#17').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#17').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#17').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#17').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#17').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#17').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#17').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#17').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#17').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#17').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#17').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#17').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#17').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#17').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#17').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#17').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#17').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#17').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#17').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#17').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#17').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#17').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#17').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#17').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#17').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#17').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#17').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#17').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#17').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#17').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 16.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#175').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#175').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#175').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#175').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#175').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#175').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#175').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#175').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#175').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#175').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#175').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#175').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#175').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#175').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#175').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#175').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#175').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#175').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#175').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#175').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#175').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#175').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#175').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#175').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#175').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#175').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#175').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#175').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#175').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#175').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#175').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#175').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#18').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#18').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#18').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#18').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#18').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#18').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#18').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#18').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#18').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#18').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#18').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#18').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#18').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#18').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#18').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#18').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#18').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#18').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#18').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#18').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#18').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#18').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#18').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#18').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#18').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#18').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#18').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#18').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#18').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#18').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#18').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#18').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 17.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#185').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#185').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#185').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#185').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#185').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#185').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#185').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#185').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#185').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#185').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#185').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#185').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#185').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#185').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#185').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#185').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#185').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#185').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#185').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#185').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#185').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#185').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#185').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#185').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#185').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#185').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#185').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#185').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#185').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#185').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#185').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#185').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#19').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#19').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#19').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#19').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#19').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#19').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#19').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#19').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#19').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#19').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#19').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#19').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#19').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#19').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#19').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#19').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#19').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#19').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#19').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#19').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#19').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#19').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#19').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#19').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#19').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#19').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#19').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#19').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#19').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#19').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#19').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#19').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 18.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#195').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#195').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#195').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#195').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#195').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#195').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#195').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#195').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#195').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#195').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#195').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#195').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#195').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#195').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#195').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#195').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#195').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#195').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#195').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#195').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#195').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#195').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#195').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#195').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#195').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#195').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#195').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#195').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#195').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#195').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#195').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#195').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#20').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#20').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#20').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#20').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#20').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#20').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#20').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#20').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#20').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#20').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#20').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#20').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#20').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#20').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#20').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#20').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#20').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#20').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#20').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#20').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#20').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#20').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#20').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#20').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#20').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#20').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#20').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#20').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#20').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#20').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#20').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#20').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 19.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#205').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#205').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#205').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#205').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#205').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#205').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#205').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#205').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#205').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#205').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#205').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#205').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#205').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#205').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#205').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#205').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#205').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#205').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#205').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#205').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#205').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#205').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#205').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#205').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#205').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#205').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#205').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#205').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#205').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#205').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#205').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#205').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#21').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#21').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#21').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#21').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#21').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#21').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#21').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#21').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#21').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#21').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#21').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#21').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#21').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#21').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#21').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#21').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#21').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#21').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#21').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#21').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#21').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#21').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#21').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#21').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#21').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#21').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#21').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#21').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#21').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#21').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#21').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#21').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 20.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#215').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#215').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#215').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#215').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#215').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#215').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#215').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#215').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#215').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#215').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#215').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#215').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#215').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#215').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#215').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#215').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#215').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#215').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#215').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#215').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#215').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#215').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#215').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#215').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#215').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#215').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#215').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#215').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#215').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#215').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#215').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#215').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#22').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#22').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#22').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#22').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#22').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#22').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#22').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#22').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#22').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#22').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#22').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#22').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#22').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#22').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#22').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#22').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#22').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#22').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#22').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#22').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#22').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#22').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#22').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#22').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#22').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#22').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#22').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#22').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#22').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#22').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#22').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#22').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 21.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#225').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#225').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#225').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#225').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#225').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#225').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#225').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#225').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#225').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#225').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#225').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#225').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#225').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#225').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#225').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#225').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#225').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#225').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#225').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#225').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#225').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#225').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#225').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#225').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#225').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#225').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#225').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#225').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#225').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#225').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#225').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#225').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#23').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#23').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#23').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#23').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#23').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#23').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#23').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#23').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#23').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#23').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#23').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#23').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#23').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#23').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#23').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#23').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#23').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#23').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#23').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#23').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#23').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#23').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#23').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#23').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#23').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#23').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#23').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#23').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#23').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#23').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#23').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#23').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 22.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#235').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#235').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#235').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#235').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#235').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#235').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#235').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#235').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#235').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#235').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#235').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#235').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#235').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#235').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#235').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#235').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#235').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#235').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#235').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#235').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#235').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#235').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#235').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#235').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#235').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#235').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#235').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#235').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#235').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#235').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#235').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#235').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#24').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#24').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#24').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#24').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#24').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#24').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#24').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#24').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#24').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#24').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#24').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#24').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#24').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#24').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#24').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#24').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#24').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#24').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#24').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#24').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#24').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#24').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#24').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#24').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#24').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#24').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#24').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#24').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#24').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#24').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#24').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#24').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                    case 23.5:
                        switch (value.end - value.start) {
                        case 0.5:
                            $('#245').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1:
                            $('#245').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 1.5:
                            $('#245').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2:
                            $('#245').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 2.5:
                            $('#245').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3:
                            $('#245').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 3.5:
                            $('#245').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4:
                            $('#245').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 4.5:
                            $('#245').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5:
                            $('#245').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 5.5:
                            $('#245').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6:
                            $('#245').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 6.5:
                            $('#245').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7:
                            $('#245').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 7.5:
                            $('#245').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8:
                            $('#245').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 8.5:
                            $('#245').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9:
                            $('#245').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 9.5:
                            $('#245').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10:
                            $('#245').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 10.5:
                            $('#245').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11:
                            $('#245').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 11.5:
                            $('#245').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12:
                            $('#245').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 12.5:
                            $('#245').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13:
                            $('#245').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 13.5:
                            $('#245').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14:
                            $('#245').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 14.5:
                            $('#245').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15:
                            $('#245').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 15.5:
                            $('#245').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        case 16:
                            $('#245').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                            break;
                        }
                        break;
                }
            });
        }) ;
      });

    $("#form-data2").submit(function(e) {
        e.preventDefault();

        var route = $("#form-data2").data("route");
        var form_data = $(this);
        var date = $("#datepicker2").val();
        var username= $("#datepicker").attr('name');
        var start = $("#start").val();
        var end = $("#end").val();
        var room = $("#room").val();
        var current_url = document.URL;

        //-- Convert Date
        var dateBookArr = date.split('-');
        var dateConvert = dateBookArr[2].concat('-' + dateBookArr[1] + '-').concat(dateBookArr[0]);

        // Hidden Modal
        function hide_mainModal(){
            $("#exampleModal").modal('hide');
            $(".modal-backdrop").remove();
        }
        
        function hide_modal(){
            $("#alert-modal").modal('hide');
            $(".modal-backdrop").remove();
        }

        // Check time
        var ctime = end - start;

        if(ctime <= 0){
            return(
                hide_mainModal(),
                $(".alert-content").html('<i class="fas fa-times fa-2x" style="color: red; right: 90px; top: 68px;"></i><p>Time Incorrect</p>'),
                $("#alert-modal").modal('show'),
                window.setTimeout(hide_modal, 2000)
            );
        }else{ 

            //-- Check Booked
            var eventArr = new Array();

            $.get(''+current_url+'/'+dateConvert+'', function (data) {
                $.each(data, function(index, value) {
                    var names = value.event;
                    var nameArr = names.split(',');
                    $.each(nameArr, function(index, value) {
                        eventArr.push(value);
                    });
                })

                var text = '';
                var i = new Number(start);
                while (i <= end) {
                    text += "," + i;
                    i += 0.5;
                }
                
                var bookArr2 = text.split(',');
                var endBooked = false;
                bookArr2.splice(-1,1)
                bookArr2.shift();

                $.each(bookArr2, function(index, book) {
                    if(eventArr.includes(book)){
                        endBooked = true;
                    }
                });

                var booked = eventArr.includes(start);
                if(booked || endBooked){
                    $(".alert-content").html('<i class="fas fa-times fa-2x" style="color: red; right: 65px; top: 68px;"></i><p>This time booked</p>')
                    $("#alert-modal").modal('show');
                    window.setTimeout(hide_modal, 2000),
                    window.setTimeout(hide_mainModal, 250);
                }else{
                        $.ajax({
                            type: "POST",
                            url: route,
                            data: form_data.serialize(),
                            success: function( msg ) {
                                $(".alert-content").html('<i class="far fa-check-circle fa-2x" style="color: green; right: 115px; top: 68px;"></i><p>Successfull</p>')
                                $("#alert-modal").modal('show');
                                window.setTimeout(hide_modal, 1500);
                            },
                            error: function(httpObj, textStatus) { 
                                if (httpObj.status==400) {
                                    window.location.href = "/login";
                                } else if (httpObj.status==500){
                                    $(".alert-content").html('<i class="fas fa-times fa-2x" style="color: red; right: 65px; top: 68px;"></i><p>Failed</p>')
                                    $("#alert-modal").modal('show');
                                    window.setTimeout(hide_modal, 2000);
                                }
                            }
                        });

                        console.log(eventArr);

                        // Chang Events
                        var tour_id = dateConvert;
                        $('#9').html('');
                        $('#95').html('');
                        $('#10').html('');
                        $('#105').html('');
                        $('#11').html('');
                        $('#115').html('');
                        $('#12').html('');
                        $('#125').html('');
                        $('#13').html('');
                        $('#135').html('');
                        $('#14').html('');
                        $('#145').html('');
                        $('#15').html('');
                        $('#155').html('');
                        $('#16').html('');
                        $('#165').html('');
                        $('#17').html('');
                        $('#175').html('');
                        $('#18').html('');
                        $('#185').html('');
                        $('#19').html('');
                        $('#195').html('');
                        $('#20').html('');
                        $('#205').html('');
                        $('#21').html('');
                        $('#215').html('');
                        $('#22').html('');
                        $('#225').html('');
                        $('#23').html('');
                        $('#235').html('');
                        $('#24').html('');
                        $('#245').html('');
                        
                        function RederLists() {
                        $.get(current_url + "/" + tour_id, function(data) {
                            //success data
                            $("#datepicker").val(date);
                            console.log(data);
                            $.each(data, function(index, value) {
                                switch (value.start) {
                                    case 8:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#9').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#9').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#9').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#9').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#9').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#9').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#9').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#9').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#9').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#9').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#9').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#9').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#9').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#9').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#9').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#9').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#9').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#9').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#9').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#9').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#9').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#9').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#9').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#9').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#9').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#9').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#9').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#9').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#9').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#9').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#9').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#9').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                    }
                                    break;
                                    case 8.5:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#95').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#95').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#95').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#95').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#95').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#95').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#95').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#95').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#95').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#95').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#95').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#95').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#95').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#95').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#95').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#95').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#95').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#95').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#95').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#95').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#95').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#95').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#95').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#95').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#95').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#95').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#95').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#95').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#95').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#95').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#95').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#95').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                    }
                                    break;
                                    case 9:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#10').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#10').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#10').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#10').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#10').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#10').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#10').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#10').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#10').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#10').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#10').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#10').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#10').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#10').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#10').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#10').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#10').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#10').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#10').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#10').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#10').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#10').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#10').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#10').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#10').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#10').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#10').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#10').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#10').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#10').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#10').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#10').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                    }
                                    break;
                                    case 9.5:
                                        switch (value.end - value.start) {
                                            case 0.5:
                                                $('#105').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 1:
                                                $('#105').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 1.5:
                                                $('#105').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 2:
                                                $('#105').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 2.5:
                                                $('#105').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 3:
                                                $('#105').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 3.5:
                                                $('#105').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 4:
                                                $('#105').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 4.5:
                                                $('#105').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 5:
                                                $('#105').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 5.5:
                                                $('#105').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                        case 6:
                                            $('#105').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#105').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#105').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#105').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#105').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#105').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#105').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#105').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#105').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#105').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#105').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#105').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#105').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#105').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#105').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#105').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#105').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#105').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#105').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#105').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#105').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 10:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#11').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#11').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#11').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#11').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#11').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#11').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#11').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#11').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#11').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#11').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#11').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#11').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#11').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#11').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#11').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#11').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#11').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#11').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#11').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#11').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#11').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#11').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#11').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#11').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#11').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#11').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#11').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#11').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#11').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#11').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#11').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#11').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                    }
                                    break;
                                    case 10.5:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#115').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#115').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#115').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#115').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#115').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#115').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#115').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#115').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#115').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#115').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#115').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#115').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#115').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#115').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#115').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#115').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#115').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#115').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#115').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#115').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#115').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#115').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#115').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#115').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#115').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#115').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#115').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#115').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#115').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#115').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#115').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#115').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                    }
                                    break;
                                    case 11:
                                        switch (value.end - value.start) {
                                            case 0.5:
                                                $('#12').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 1:
                                                $('#12').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 1.5:
                                                $('#12').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 2:
                                                $('#12').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 2.5:
                                                $('#12').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 3:
                                                $('#12').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 3.5:
                                                $('#12').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 4:
                                                $('#12').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 4.5:
                                                $('#12').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 5:
                                                $('#12').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                            case 5.5:
                                                $('#12').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                                break;
                                        case 6:
                                            $('#12').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#12').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#12').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#12').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#12').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#12').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#12').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#12').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#12').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#12').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#12').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#12').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#12').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#12').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#12').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#12').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#12').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#12').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#12').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#12').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#12').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 11.5:
                                    switch (value.end - value.start) {
                                        case 0.5:
                                            $('#125').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#125').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#125').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#125').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#125').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#125').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#125').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#125').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#125').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#125').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#125').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#125').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#125').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#125').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#125').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#125').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#125').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#125').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#125').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#125').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#125').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#125').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#125').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#125').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#125').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#125').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#125').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#125').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#125').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#125').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#125').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#125').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 12:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#13').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#13').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#13').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#13').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#13').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#13').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#13').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#13').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#13').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#13').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#13').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#13').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#13').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#13').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#13').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#13').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#13').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#13').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#13').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#13').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#13').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#13').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#13').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#13').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#13').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#13').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#13').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#13').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#13').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#13').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#13').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#13').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 12.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#135').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#135').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#135').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#135').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#135').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#135').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#135').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#135').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#135').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#135').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#135').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#135').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#135').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#135').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#135').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#135').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#135').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#135').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#135').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#135').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#135').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#135').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#135').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#135').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#135').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#135').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#135').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#135').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#135').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#135').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#135').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#135').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 13:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#14').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#14').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#14').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#14').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#14').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#14').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#14').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#14').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#14').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#14').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#14').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#14').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#14').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#14').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#14').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#14').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#14').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#14').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#14').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#14').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#14').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#14').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#14').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#14').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#14').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#14').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#14').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#14').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#14').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#14').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#14').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#14').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 13.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#145').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#145').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#145').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#145').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#145').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#145').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#145').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#145').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#145').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#145').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#145').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#145').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#145').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#145').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#145').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#145').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#145').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#145').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#145').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#145').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#145').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#145').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#145').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#145').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#145').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#145').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#145').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#145').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#145').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#145').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#145').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#145').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 14:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#15').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#15').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#15').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#15').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#15').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#15').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#15').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#15').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#15').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#15').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#15').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#15').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#15').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#15').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#15').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#15').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#15').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#15').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#15').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#15').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#15').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#15').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#15').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#15').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#15').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#15').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#15').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#15').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#15').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#15').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#15').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#15').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 14.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#155').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#155').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#155').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#155').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#155').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#155').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#155').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#155').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#155').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#155').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#155').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#155').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#155').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#155').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#155').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#155').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#155').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#155').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#155').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#155').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#155').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#155').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#155').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#155').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#155').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#155').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#155').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#155').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#155').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#155').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#155').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#155').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 15:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#16').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#16').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#16').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#16').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#16').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#16').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#16').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#16').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#16').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#16').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#16').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#16').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#16').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#16').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#16').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#16').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#16').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#16').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#16').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#16').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#16').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#16').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#16').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#16').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#16').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#16').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#16').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#16').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#16').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#16').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#16').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#16').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 15.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#165').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#165').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#165').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#165').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#165').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#165').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#165').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#165').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#165').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#165').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#165').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#165').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#165').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#165').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#165').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#165').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#165').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#165').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#165').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#165').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#165').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#165').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#165').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#165').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#165').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#165').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#165').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#165').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#165').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#165').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#165').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#165').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 16:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#17').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#17').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#17').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#17').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#17').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#17').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#17').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#17').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#17').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#17').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#17').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#17').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#17').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#17').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#17').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#17').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#17').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#17').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#17').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#17').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#17').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#17').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#17').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#17').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#17').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#17').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#17').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#17').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#17').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#17').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#17').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#17').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 16.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#175').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#175').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#175').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#175').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#175').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#175').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#175').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#175').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#175').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#175').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#175').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#175').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#175').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#175').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#175').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#175').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#175').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#175').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#175').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#175').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#175').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#175').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#175').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#175').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#175').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#175').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#175').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#175').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#175').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#175').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#175').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#175').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 17:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#18').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#18').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#18').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#18').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#18').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#18').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#18').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#18').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#18').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#18').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#18').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#18').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#18').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#18').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#18').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#18').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#18').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#18').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#18').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#18').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#18').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#18').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#18').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#18').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#18').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#18').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#18').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#18').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#18').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#18').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#18').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#18').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 17.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#185').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#185').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#185').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#185').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#185').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#185').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#185').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#185').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#185').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#185').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#185').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#185').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#185').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#185').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#185').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#185').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#185').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#185').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#185').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#185').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#185').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#185').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#185').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#185').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#185').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#185').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#185').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#185').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#185').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#185').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#185').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#185').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 18:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#19').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#19').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#19').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#19').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#19').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#19').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#19').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#19').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#19').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#19').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#19').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#19').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#19').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#19').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#19').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#19').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#19').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#19').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#19').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#19').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#19').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#19').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#19').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#19').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#19').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#19').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#19').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#19').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#19').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#19').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#19').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#19').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 18.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#195').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#195').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#195').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#195').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#195').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#195').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#195').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#195').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#195').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#195').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#195').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#195').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#195').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#195').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#195').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#195').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#195').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#195').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#195').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#195').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#195').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#195').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#195').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#195').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#195').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#195').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#195').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#195').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#195').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#195').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#195').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#195').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 19:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#20').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#20').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#20').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#20').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#20').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#20').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#20').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#20').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#20').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#20').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#20').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#20').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#20').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#20').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#20').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#20').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#20').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#20').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#20').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#20').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#20').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#20').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#20').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#20').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#20').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#20').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#20').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#20').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#20').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#20').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#20').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#20').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 19.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#205').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#205').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#205').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#205').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#205').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#205').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#205').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#205').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#205').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#205').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#205').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#205').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#205').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#205').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#205').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#205').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#205').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#205').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#205').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#205').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#205').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#205').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#205').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#205').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#205').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#205').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#205').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#205').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#205').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#205').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#205').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#205').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 20:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#21').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#21').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#21').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#21').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#21').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#21').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#21').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#21').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#21').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#21').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#21').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#21').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#21').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#21').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#21').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#21').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#21').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#21').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#21').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#21').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#21').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#21').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#21').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#21').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#21').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#21').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#21').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#21').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#21').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#21').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#21').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#21').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 20.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#215').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#215').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#215').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#215').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#215').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#215').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#215').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#215').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#215').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#215').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#215').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#215').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#215').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#215').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#215').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#215').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#215').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#215').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#215').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#215').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#215').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#215').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#215').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#215').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#215').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#215').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#215').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#215').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#215').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#215').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#215').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#215').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 21:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#22').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#22').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#22').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#22').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#22').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#22').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#22').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#22').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#22').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#22').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#22').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#22').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#22').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#22').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#22').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#22').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#22').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#22').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#22').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#22').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#22').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#22').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#22').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#22').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#22').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#22').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#22').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#22').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#22').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#22').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#22').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#22').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 21.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#225').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#225').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#225').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#225').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#225').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#225').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#225').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#225').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#225').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#225').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#225').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#225').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#225').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#225').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#225').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#225').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#225').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#225').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#225').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#225').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#225').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#225').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#225').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#225').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#225').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#225').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#225').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#225').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#225').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#225').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#225').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#225').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 22:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#23').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#23').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#23').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#23').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#23').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#23').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#23').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#23').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#23').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#23').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#23').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#23').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#23').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#23').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#23').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#23').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#23').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#23').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#23').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#23').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#23').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#23').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#23').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#23').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#23').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#23').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#23').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#23').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#23').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#23').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#23').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#23').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 22.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#235').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#235').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#235').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#235').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#235').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#235').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#235').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#235').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#235').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#235').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#235').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#235').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#235').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#235').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#235').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#235').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#235').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#235').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#235').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#235').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#235').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#235').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#235').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#235').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#235').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#235').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#235').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#235').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#235').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#235').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#235').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#235').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 23:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#24').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#24').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#24').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#24').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#24').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#24').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#24').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#24').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#24').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#24').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#24').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#24').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#24').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#24').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#24').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#24').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#24').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#24').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#24').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#24').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#24').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#24').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#24').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#24').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#24').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#24').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#24').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#24').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#24').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#24').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#24').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#24').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                    case 23.5:
                                        switch (value.end - value.start) {
                                        case 0.5:
                                            $('#245').html('<div class="book0-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1:
                                            $('#245').html('<div class="book1" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 1.5:
                                            $('#245').html('<div class="book1-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2:
                                            $('#245').html('<div class="book2" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 2.5:
                                            $('#245').html('<div class="book2-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3:
                                            $('#245').html('<div class="book3" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 3.5:
                                            $('#245').html('<div class="book3-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4:
                                            $('#245').html('<div class="book4" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 4.5:
                                            $('#245').html('<div class="book4-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5:
                                            $('#245').html('<div class="book5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 5.5:
                                            $('#245').html('<div class="book5-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6:
                                            $('#245').html('<div class="book6" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 6.5:
                                            $('#245').html('<div class="book6-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7:
                                            $('#245').html('<div class="book7" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 7.5:
                                            $('#245').html('<div class="book7-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8:
                                            $('#245').html('<div class="book8" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 8.5:
                                            $('#245').html('<div class="book8-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9:
                                            $('#245').html('<div class="book9" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 9.5:
                                            $('#245').html('<div class="book9-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10:
                                            $('#245').html('<div class="book10" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 10.5:
                                            $('#245').html('<div class="book10-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11:
                                            $('#245').html('<div class="book11" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 11.5:
                                            $('#245').html('<div class="book11-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12:
                                            $('#245').html('<div class="book12" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 12.5:
                                            $('#245').html('<div class="book12-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13:
                                            $('#245').html('<div class="book13" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 13.5:
                                            $('#245').html('<div class="book13-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14:
                                            $('#245').html('<div class="book14" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 14.5:
                                            $('#245').html('<div class="book14-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15:
                                            $('#245').html('<div class="book15" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 15.5:
                                            $('#245').html('<div class="book15-5" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        case 16:
                                            $('#245').html('<div class="book16" id="'+ value.id +'" name="'+ username +'"><p>'+ value.subject +'</p></div>');
                                            break;
                                        }
                                        break;
                                }
                            });
                        });
                        }

                        setTimeout(RederLists, 1250);

                        // Hidden Modal
                        hide_mainModal()
                }
            });
        }
    });

    $(document).on('click','.res-login',function(event){
        var current_url = document.URL;
        var current_urlArr = current_url.split('/');
        var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];

        var form = $('<form action="' + url + '" method="get">' +
        '<input type="text" name="alert" value="login" />' +
        '</form>');
        $('body').append(form);
        form.submit();
    });

});