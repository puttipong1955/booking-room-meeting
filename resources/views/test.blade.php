<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    {{-- <div id="messages"></div>
            <form id="form-data" data-route="{{route('postData')}}" method="POST">
                {{csrf_field()}}
                <label>Name :</label>   
                <input type="text" name="person_name"><br>
                <label>E-mail :</label>   
                <input type="text" name="person_email"><br>
                <button type="submit">Submit</button>
            </form>
            <form id="form-data2" data-route="{{route('postData2')}}" method="POST">
                {{csrf_field()}}
                <label>subject :</label>   
                <input type="text" name="subject" id="subject"><br>
                <label>date :</label>   
                <input type="text" name="date" id="date"><br>
                <label>start :</label>   
                <input type="text" name="start" id="start"><br>
                <label>end :</label>   
                <input type="text" name="end" id="end"><br>
                <input type="hidden" name="user_id" id="user_id" value="1">
                <input type="hidden" name="room" id="room" value="s">
                <button type="submit">Submit</button>
            </form>
        <script src="https://code.jquery.com/jquery-3.4.1.js" type="text/javascript"></script>
        <script src="{{ asset('js/post.js') }}" type="text/javascript"></script>
        <script>
            $(function() {
                $("#form-data").submit(function(e) {
                    var route = $("#form-data").data("route");
                    var form_data = $(this);

                    $.ajax({
                        type: "POST",
                        url: route,
                        data: form_data.serialize(),
                        success: function(Response) {
                            console.log(Response);
                            if (Response.person_name) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.person_name + "</p>"
                                );
                            }

                            if (Response.person_email) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.person_email + "</p>"
                                );
                            }

                            if (Response.success) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.success + "</p>"
                                );
                            }
                        }
                    });

                    e.preventDefault();
                });
            });
            $(function() {
                $("#form-data2").submit(function(e) {
                    var route = $("#form-data2").data("route");
                    var form_data = $(this);

                    $.ajax({
                        type: "POST",
                        url: route,
                        data: form_data.serialize(),
                        success: function(Response) {
                            console.log(Response);
                            if (Response.person_name) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.person_name + "</p>"
                                );
                            }

                            if (Response.person_email) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.person_email + "</p>"
                                );
                            }

                            if (Response.success) {
                                $("#messages").append(
                                    '<p class="alert">' + Response.success + "</p>"
                                );
                            }
                        }
                    });

                    e.preventDefault();
                });
            });
        </script> --}}
        {{-- <div class="container">
            <div class="row">
                <div class='col-sm-6'>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker1').datetimepicker();
                    });
                </script>
            </div>
        </div> --}}
        <input id="datepicker" width="276" />
        <script>
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mm-yyyy'
            });
        </script>
    </body>
</html>