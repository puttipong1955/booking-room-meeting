<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Room L</title>
    <link rel="stylesheet" href="{{URL::asset('css/room.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Dosis|Poppins|Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- jQuery --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{URL::asset('js/bootstrap.js')}}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <!-- Modal -->
    <div class="modal fade modalForm" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
           <form id="form-data2" data-route="{{route('postData2')}}" method="POST">
            {{csrf_field()}}
            <div class="card text-dark bg-white">
            <div class="card-header" >
                <h5 class="card-title">L - Room</h5>
                <i class="fas fa-times-circle fa-2x" data-dismiss="modal"></i>
            </div>
            <div class="card-body">
              <input type="hidden" id="room" name="room" value="l"/>
              <input type="hidden" id="user_id" name="user_id" value="@if(Auth::user()) {{Auth::user()->id}}@endif"/>
              <input type="hidden" id="id" name="id" value=""/>
              <ul>
                <li>
                  Subject
                  <div class="obj"><input id="subject" name="subject" type="text" required maxlength="255"></div>
                </li>
                <li>
                  Date
                  <div class="obj2">
                    <input id="datepicker2" name="date" width="210" value="<?php echo date('d-m-Y'); ?>" readonly required/>
                    <p class="date-label" style="position: absolute; width: 300px; height: 50px; margin-top: -26.5px; background: white; display: none"></p>
                  </div>
                </li>
                <li>
                  Start
                  <div class="obj3">
                    <select style="border: none;" id="start" name="start" >
                      <option value="8" selected>8:00</option>
                      <option value="8.5">8:30</option>
                      <option value="9">9:00</option>
                      <option value="9.5">9:30</option>
                      <option value="10">10:00</option>
                      <option value="10.5">10:30</option>
                      <option value="11">11:00</option>
                      <option value="11.5">11:30</option>
                      <option value="12">12:00</option>
                      <option value="12.5">12:30</option>
                      <option value="13">13:00</option>
                      <option value="13.5">13:30</option>
                      <option value="14">14:00</option>
                      <option value="14.5">14:30</option>
                      <option value="15">15:00</option>
                      <option value="15.5">15:30</option>
                      <option value="16">16:00</option>
                      <option value="16.5">16:30</option>
                      <option value="17">17:00</option>
                      <option value="17.5">17:30</option>
                      <option value="18">18:00</option>
                      <option value="18.5">18:30</option>
                      <option value="19">19:00</option>
                      <option value="19.5">19:30</option>
                      <option value="20">20:00</option>
                      <option value="20.5">20:30</option>
                      <option value="21">21:00</option>
                      <option value="21.5">21:30</option>
                      <option value="22">22:00</option>
                      <option value="22.5">22:30</option>
                      <option value="23">23:00</option>
                      <option value="23.5">23:30</option>
                   </select>
                  </div>
                </li>
                <li>
                  End
                  <div class="obj4">
                    <select style="border: none;" id="end" name="end">
                        <option value="8.5" selected>8:30</option>
                        <option value="9">9:00</option>
                        <option value="9.5">9:30</option>
                        <option value="10">10:00</option>
                        <option value="10.5">10:30</option>
                        <option value="11">11:00</option>
                        <option value="11.5">11:30</option>
                        <option value="12">12:00</option>
                        <option value="12.5">12:30</option>
                        <option value="13">13:00</option>
                        <option value="13.5">13:30</option>
                        <option value="14">14:00</option>
                        <option value="14.5">14:30</option>
                        <option value="15">15:00</option>
                        <option value="15.5">15:30</option>
                        <option value="16">16:00</option>
                        <option value="16.5">16:30</option>
                        <option value="17">17:00</option>
                        <option value="17.5">17:30</option>
                        <option value="18">18:00</option>
                        <option value="18.5">18:30</option>
                        <option value="19">19:00</option>
                        <option value="19.5">19:30</option>
                        <option value="20">20:00</option>
                        <option value="20.5">20:30</option>
                        <option value="21">21:00</option>
                        <option value="21.5">21:30</option>
                        <option value="22">22:00</option>
                        <option value="22.5">22:30</option>
                        <option value="23">23:00</option>
                        <option value="23.5">23:30</option>
                        <option value="24">00:00</option>
                   </select>
                  </div>
                </li>
                <li>
                  By
                  <div class="obj5"><p id="username" name="username">@if(Auth::user()) {{Auth::user()->name}} @endif</p></div>
                </li>
              </ul>
            </div>
            @if(Auth::user())
                <div class="card-footer bg-transparent text-center">
                    <button class="btn" id="btn" type="submit"><span>Add Book</span></button>
                    <button class="btn del-event" name="{{Auth::user()->id}}" data-dismiss="modal" style="display: none"><span>Delete Book</span></button>
                </div>
            @endif
            </div>
           </form>
        </div>
      </div>
    </div>

    <div class="modal fade modalForm" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="top: 150px;">
          <div class="modal-content text-center alert-content" style="height: 200px; font-size: 1.3rem; padding-top: 60px"></div>
        </div>
    </div>

    <!-- Content -->
    <div class="container-fluid">        
        <div class="calendar-day mobile">
          <div class="row">
          <div class="card-header">
            
            <div class="home">
                <a href="{{route('index')}}"><i class="fas fa-home fa-lg"></i></a></i>
            </div>
            <div class="add-event">
                @if(Auth::user())
                    <i class="clear add fas fa-plus-circle fa-lg" name="{{Auth::user()->name}}" data-toggle="modal" data-target="#exampleModal"></i>
                @else
                    <i class="res-login fas fa-plus-circle fa-lg" data-dismiss="modal"></i>
                @endif
            </div><p class="head-name">L - Room</p>
            @if(Auth::user())
              <input id="datepicker" class="date-head" name="{{Auth::user()->id}}" value="<?php echo date('d-m-Y'); ?>" readonly/>
            @else
              <input id="datepicker" class="date-head" value="<?php echo date('d-m-Y'); ?>" readonly/>
            @endif
          </div>
          
          <div class="col-2 col-time" style="margin: 0 auto;">
            <div class="time">8.00</div>
            <div class="time">8.30</div>
            <div class="time">9.00</div>
            <div class="time">9.30</div>
            <div class="time">10.00</div>
            <div class="time">10.30</div>
            <div class="time">11.00</div>
            <div class="time">11.30</div>
            <div class="time">12.00</div>
            <div class="time">12.30</div>
            <div class="time">13.00</div>
            <div class="time">13.30</div>
            <div class="time">14.00</div>
            <div class="time">14.30</div>
            <div class="time">15.00</div>
            <div class="time">15.30</div>
            <div class="time">16.00</div>
            <div class="time">16.30</div>
            <div class="time">17.00</div>
            <div class="time">17.30</div>
            <div class="time">18.00</div>
            <div class="time">18.30</div>
            <div class="time">19.00</div>
            <div class="time">19.30</div>
            <div class="time">20.00</div>
            <div class="time">20.30</div>
            <div class="time">21.00</div>
            <div class="time">21.30</div>
            <div class="time">22.00</div>
            <div class="time">22.30</div>
            <div class="time">23.00</div>
            <div class="time">23.30</div>
          </div>
          <div class="col-10 col-event" style="padding: 0; margin: 0;">
            <div class="event" id="9"></div>
            <div class="event" id="95"></div>
            <div class="event" id="10"></div>
            <div class="event" id="105"></div>
            <div class="event" id="11"></div>
            <div class="event" id="115"></div>
            <div class="event" id="12"></div>
            <div class="event" id="125"></div>
            <div class="event" id="13"></div>
            <div class="event" id="135"></div>
            <div class="event" id="14"></div>
            <div class="event" id="145"></div>
            <div class="event" id="15"></div>
            <div class="event" id="155"></div>
            <div class="event" id="16"></div>
            <div class="event" id="165"></div>
            <div class="event" id="17"></div>
            <div class="event" id="175"></div>
            <div class="event" id="18"></div>
            <div class="event" id="185"></div>
            <div class="event" id="19"></div>
            <div class="event" id="195"></div>
            <div class="event" id="20"></div>
            <div class="event" id="205"></div>
            <div class="event" id="21"></div>
            <div class="event" id="215"></div>
            <div class="event" id="22"></div>
            <div class="event" id="225"></div>
            <div class="event" id="23"></div>
            <div class="event" id="235"></div>
            <div class="event" id="24"></div>
            <div class="event" id="245"></div>
          </div>
          </div>
        </div>
    </div> 
    <script src="{{URL::asset('js/room-event.js')}}">
    <script>
        var msg = '{{Session::get('alert')}}';
        var exist = '{{Session::has('alert')}}';
        if(exist){
          alert(msg);
        }
      </script>
</body>
</html>