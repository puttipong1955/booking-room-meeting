<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Booking Meeting Room</title>
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Dosis|Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Prompt&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">    
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{URL::asset('js/bootstrap.js')}}"></script>
</head>
<body>
   <!-- Modal Add Form-->
   <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="top: 150px;">
      <div class="modal-content"> 
        @if(Auth::user())
          {{-- Form profile --}}
          <form class="profile-user" id="profile-user" method="POST" data-route="{{route('editUser')}}">
            {{csrf_field()}}
            <input type="hidden" name="id" id="id" value="{{Auth::user()->id}}"/>
            <div class="card text-dark bg-white">
            <div class="card-body">
              <ul>
                <li>
                  <h5 style="color: rgb(29, 29, 31); font-family: 'Poppins', sans-serif; padd-left: -20px; font-size: 1.5rem">
                    Profile
                  </h5>
                </li>
                <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Email</span>
                    <input class="email @error('email') is-invalid @enderror" id="email" name="email" type="email" value="{{Auth::user()->email}}" required autocomplete="email" autofocus>
                    @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                </li>
                <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Name</span>
                    <input class="username @error('name') is-invalid @enderror" id="username" name="username" type="text" value="{{Auth::user()->name}}" required autocomplete="name">
                    @error('name')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                </li>
                <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">New Password</span>
                    <input class="password @error('password') is-invalid @enderror" id="password" name="password" type="password" required autocomplete="new-password" minlength="8">
                    @error('password')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                </li>
                <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Confirm New Password</span>
                    <input class="password2" id="password-confirm" name="password_confirmation" type="password" required autocomplete="new-password"  minlength="8">
                    <p class="delLabel" id="{{Auth::user()->id}}">
                      <span>Don't want an account?</span> 
                      Delete
                    </p>
                </li>
                <li>
                  <button class="btnSubmit" id="btn" type="submit"><span>{{ __('Save') }}</span></button>
                </li>
              </ul>
            </div>
          </div>
          </form>
          @endif

           {{-- Form register --}}
            <form class="add-user" id="add-user" method="POST" action="{{ route('register') }}">
              @csrf
              <input type="hidden" name="id" id="id"/>
              <div class="card text-dark bg-white">
              <div class="card-body">
                <ul>
                  <li>
                    <h5 style="color: rgb(29, 29, 31); font-family: 'Poppins', sans-serif; padd-left: -20px; font-size: 1.5rem">
                      Sign Up
                    </h5>
                  </li>
                  <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Email</span>
                      <input class="email @error('email') is-invalid @enderror" id="email" name="email" type="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                      @error('email')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </li>
                  <li>
                      <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Name</span>
                      <input class="username @error('name') is-invalid @enderror" id="name" name="name" type="text" required autocomplete="name">
                      @error('name')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </li>
                  <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Password</span>
                      <input class="password @error('password') is-invalid @enderror" id="password" name="password" type="password" required autocomplete="new-password">
                      @error('password')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                  </li>
                  <li>
                      <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Confirm Password</span>
                      <input class="password2" id="password-confirm" name="password_confirmation" type="password" required autocomplete="new-password">
                      <p class="loginLabel">
                        <span>Have an account?</span> 
                        Sign in
                      </p>
                  </li>
                  <li>
                    <button class="btnSubmit" id="btn" type="submit"><span>{{ __('Sing up') }}</span></button>
                  </li>
                </ul>
              </div>
            </div>
            </form>

            {{-- Form login --}}
            <form class="login-user" id="login-user" action="{{ route('login') }}" method="POST">
              @csrf
              <input type="hidden" name="id" id="id"/>
              <div class="card text-dark bg-white">
              <div class="card-body">
                <ul class="text-center">
                  <li>
                    <h5 style="color: rgb(29, 29, 31); font-family: 'Poppins', sans-serif; padd-left: -20px; font-size: 1.5rem">
                      Welcome to MRB
                    </h5>
                  </li>
                  <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Email</span> 
                    <input id="email" type="email" class="email @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                  </li>
                  <li>
                    <span style="position: absolute; margin-top: -20px; font-size: 0.85rem; color: gray">Password</span>
                    <input id="password" type="password" class="password @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                      <p class="create-profile">
                        <span>Create an account?</span> 
                        Sign up
                      </p>
                  </li>
                  <li>
                    <button class="btnSubmit" id="btn" type="submit"><span>{{ __('Sing in') }}</span></button>
                  </li>
                </ul>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>

    {{-- Modal Alert --}}
      <div class="modal fade modalForm" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="top: 150px;">
          <div class="modal-content text-center alert-content" style="height: 150px; font-size: 1.3rem; padding-top: 60px"></div>
        </div>
    </div>

  <!-- Content-->
    <div class="container-fluid">
        @if(Auth::user())
          <input type="hidden" id="profile-name"  name="profile-name" value="{{Auth::user()->email}}">
          <div class="header-profile"> 
            <i class="fas fa-user-tie" data-toggle="modal" data-target="#addModal" style="margin-right: 25px" title="Profile"><span id="user-name" style="font-size: 1.2rem; padding-left: 10px;"> {{Auth::user()->name}}</span></i>
          </div>
          <div class="header">
            <a href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt" title="Logout"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </div>
        @else
          <div class="header">
            <i class="fas fa-user-tie" data-toggle="modal" data-target="#addModal"></i>
        </div>
        @endif
        <div class="title">
            <p class="text-center"><span>M</span>eeting <span>B</span>ooking <span>R</span>oom</p>
        </div>
        <div class="button">
            <a href="{{route('sroom')}}"><button class="room-s">S - Room</button></a>
            <a href="{{route('lroom')}}"><button class="room-l">L - Room</button></a>
        </div>
    </div>
    <script src="{{URL::asset('js/home.js')}}">
    <script>
      //alert('test');
    </script>
</body>
</html>
