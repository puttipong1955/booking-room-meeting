@extends('admin/layout')
@section('title', 'Admin L-room')
@section('css')
    <link rel="stylesheet" href="{{URL::asset('css/admin-room-s.css')}}">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="{{URL::asset('js/bootstrap.js')}}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content') 
<!-- Modal Add -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form id="add-event" data-route="{{route('postData2')}}" method="POST">
          {{csrf_field()}}
            <div class="card text-dark bg-white">
            <div class="card-header" >
                <h5 class="card-title">Add Event</h5>
                <i class="close-icon fas fa-times-circle fa-2x" data-dismiss="modal"></i>
            </div>
            <div class="card-body">
              <input type="hidden" id="room" name="room" value="l"/>
              <input type="hidden" id="user_id" name="user_id" value="1"/>
              <input type="hidden" id="id" name="id" value=""/>
              <input type="hidden" id="admin" name="admin" value="1"/>
              <ul>
                <li>
                  Subject
                  <div class="obj"><input id="subject" name="subject" type="text" required></div>
                </li>
                <li>
                  Date
                  <div class="obj2"><input id="date" name="date" value="<?php echo date('d-m-Y');?>" readonly></div>
                </li>
                <li>
                  Start
                  <div class="obj3">
                    <select style="border: none;" id="start" name="start">
                      <option value="8">8:00</option>
                      <option value="8.5">8:30</option>
                      <option value="9">9:00</option>
                      <option value="9.5">9:30</option>
                      <option value="10">10:00</option>
                      <option value="10.5">10:30</option>
                      <option value="11">11:00</option>
                      <option value="11.5">11:30</option>
                      <option value="12">12:00</option>
                      <option value="12.5">12:30</option>
                      <option value="13">13:00</option>
                      <option value="13.5">13:30</option>
                      <option value="14">14:00</option>
                      <option value="14.5">14:30</option>
                      <option value="15">15:00</option>
                      <option value="15.5">15:30</option>
                      <option value="16">16:00</option>
                      <option value="16.5">16:30</option>
                      <option value="17">17:00</option>
                      <option value="17.5">17:30</option>
                      <option value="18">18:00</option>
                      <option value="18.5">18:30</option>
                      <option value="19">19:00</option>
                      <option value="19.5">19:30</option>
                      <option value="20">20:00</option>
                      <option value="20.5">20:30</option>
                      <option value="21">21:00</option>
                      <option value="21.5">21:30</option>
                      <option value="22">22:00</option>
                      <option value="22.5">22:30</option>
                      <option value="23">23:00</option>
                      <option value="23.5">23:30</option>
                   </select>
                  </div>
                </li>
                <li>
                  End
                  <div class="obj4">
                    <select style="border: none;" id="end" name="end">
                      <option value="8.5">8:30</option>
                      <option value="9">9:00</option>
                      <option value="9.5">9:30</option>
                      <option value="10">10:00</option>
                      <option value="10.5">10:30</option>
                      <option value="11">11:00</option>
                      <option value="11.5">11:30</option>
                      <option value="12">12:00</option>
                      <option value="12.5">12:30</option>
                      <option value="13">13:00</option>
                      <option value="13.5">13:30</option>
                      <option value="14">14:00</option>
                      <option value="14.5">14:30</option>
                      <option value="15">15:00</option>
                      <option value="15.5">15:30</option>
                      <option value="16">16:00</option>
                      <option value="16.5">16:30</option>
                      <option value="17">17:00</option>
                      <option value="17.5">17:30</option>
                      <option value="18">18:00</option>
                      <option value="18.5">18:30</option>
                      <option value="19">19:00</option>
                      <option value="19.5">19:30</option>
                      <option value="20">20:00</option>
                      <option value="20.5">20:30</option>
                      <option value="21">21:00</option>
                      <option value="21.5">21:30</option>
                      <option value="22">22:00</option>
                      <option value="22.5">22:30</option>
                      <option value="23">23:00</option>
                      <option value="23.5">23:30</option>
                      <option value="24">00:00</option>
                   </select>
                  </div>
                </li>
                <li>
                  By
                  <div class="obj5"><p>Admin</p></div>
                </li>
              </ul>
            </div>
              <div class="card-footer bg-transparent text-center">
                <button class="btn" id="btn" type="submit"><span>Add E-vent</span></button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>

  <!-- Modal Delete-->
 <div class="modal fade modal-del" id="delModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="top: 150px;">
      <div class="modal-content">
          <div class="card text-dark bg-white">
          <div class="card-header" ><h5 class="card-title"><i class="far fa-calendar-alt"> </i> <span id="alert-head">Delete Event</span></h5></div>
          <div id="alert-centent" class="card-body text-center">
            Are You Sure?
          </div>
          <div class="card-footer bg-transparent text-center">
            <button class="btn del-event" style="margin-left: 25%"><span>Delete Event</span></button>
          </div>
        </div>
      </div>
    </div>
  </div>

  

<p class="head">
    <i class="fas fa-users"></i>
    <i title="Add Event" class="add-far far fa-calendar-plus fa-lg" data-toggle="modal" data-target="#addModal"> </i>
     <a href="{{route("admin-lroom")}}">L - Room </a> 
    <span>Booking History</span>
</p>
<div class="search" style="display: none">
        <i class="fas fa-search"></i>
        <input type="text" name="search" id="search">
        <button>ค้นหา</button>
</div>
<div class="col col-sm-12">
    <div class="select-event" style="color: grey; font-size: 0.85rem; font-family: 'Poppins', sans-serif; padding-top: 20px;">
      <p>From</p>
      <input type="text" id="dateFrom" style="width: 100px; position: absolute; margin-top: -45px; left: 45px" readonly> 
      <p style="position: absolute; top: 22px; left: 170px">To</p> 
      <input type="text" id="dateTo" style="width: 100px; position: absolute; margin-top: -45px; left: 185px;" readonly> 
      <button style="position: absolute; top: 22px; left: 305px; border: none; background: transparent; color: rgb(1, 168, 146);" id="select-event-submit">Search</button>
    </div>
    <ul id="event-select">
      <div class="pagiante pagiante-list-select" 
             style="display: flex; align-items: center; justify-content: center; color: rgb(1, 168, 146);">
          {{-- {{ $sroomDate->links() }} --}}
      </div>
    </ul>
    <ul id="event-load">
      @foreach ($lroomDate as $item)
        <li>
            <div class="event">
                <div class="header">
                <p>
                  <i class="far fa-calendar-alt fa-xs"></i> 
                  <?php 
                    $str = explode("-",$item->date);
                    $dat = "-";
                    $dateCover = $str[2].$dat.$str[1].$dat.$str[0];
                    print_r($dateCover)
                  ?>
                </p>
                   <i title="Add Event" id="<?php print_r($dateCover)?>" class="add-event far fa-calendar-plus fa-lg"></i>
                </div>
                <div class="content">
                    <table>
                        <thead>
                            <tr class="header1">
                                <th class="bg"><i class="far fa-clock"></i> Time</th>
                                <th><i class="far fa-edit"></i> Subject</th>
                                <th><i class="far fa-user"></i> User</th>
                                <th><i class="fas fa-pen"></i> Created</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="list-event-select">
                            @foreach ($lroomEvent as $event)
                                @if ($item->date == $event->date)
                                  <tr>
                                    <td class="bg">
                                      <?php 
                                        if(strpos($event->start,".5") > 0 && strpos($event->end,".5") <= 0){
                                          $startConvert = substr($event->start,0,-2);
                                          echo $startConvert.":30 - ".$event->end.":00";
                                        } else if(strpos($event->end,".5") > 0 && strpos($event->start,".5") <= 0){
                                          $endConvert = substr($event->end,0,-2);
                                          echo $event->start.":00 - ".$endConvert.":30";
                                        } else if(strpos($event->end,".5") > 0 && strpos($event->start,".5") > 0){ 
                                          $startConvert = substr($event->start,0,-2);
                                          $endConvert = substr($event->end,0,-2);
                                          echo $startConvert.":30 - ".$endConvert.":30";
                                        } else {
                                          echo $event->start.":00 - ".$event->end.":00";
                                        }
                                      ?>
                                      
                                    </td>
                                    <td>{{$event->subject}}</td>
                                    @if($event->user_id == 1)
                                      <td>Admin</td>
                                    @else
                                      <?php
                                        foreach ($usersAll as $user) {
                                            if($event->user_id == $user->id){
                                              echo"<td>$user->name</td>";
                                            }
                                        }
                                      ?>
                                    @endif
                                    <td>{{date('d/m/Y : H:m', strtotime($event->updated_at))}}</td>
                                    <td class="manage">
                                        <i id="{{$event->id}}" class="del-alog fas fa-trash fa-xs"></i>
                                    </td>
                                </tr>   
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </li>   
        @endforeach
        <div class="pagiante" 
             style="display: flex; align-items: center; justify-content: center; color: rgb(1, 168, 146);">
          {{ $lroomDate->links() }}
        </div>
    </ul>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#date').datepicker({
            icons: {
                rightIcon: ''
            },
            format: 'dd-mm-yyyy'
        });
        $('#dateFrom').datepicker({
            icons: {
                rightIcon: ''
            },
            format: 'dd-mm-yyyy'
        });
        $('#dateTo').datepicker({
            icons: {
                rightIcon: ''
            },
            format: 'dd-mm-yyyy'
        });
      });

     $(document).on('click','.add-event',function(event){
      var date= $(this).attr('id');
      $('#date').val(date);
      $('#addModal').modal('show');
    }); 
        
     $(document).on('click','.del-event',function(event){
          var current_url = document.URL;
          var current_urlArr = current_url.split('/');
          var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];
          var urlConvert = url + '/del-event';
          var tour_id= $(this).attr('id');
          $.get(urlConvert + '/l/' + tour_id + '/1') ;
          window.setTimeout(location.reload(true), 1000);
        });  

    $(document).on('click','.del-alog',function(event){
        $('#delModal').modal('show');
        var tour_id= $(this).attr('id');
        $("#alert-centent").html('Are You Sure?');
        $("#alert-head").html('Delete Event');
        $(".del-event").css('display', 'block');
        $('.del-event').attr("id",tour_id);
        }); 

    $("#add-event").submit(function(e) {

      e.preventDefault();
        
        var route = $("#add-event").data("route");
        var form_data = $(this);
        var date = $("#date").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var room = $("#room").val();

        //-- Convert Date
        var dateBookArr = date.split('-');
        var dateConvert = dateBookArr[2].concat('-' + dateBookArr[1] + '-').concat(dateBookArr[0]);

        // Hidden Modal
        function hide_mainModal(){
            $("#addModal").modal('hide');
            $(".modal-backdrop").remove();
        }

        function hide_alertModal(){
            $("#delModal").modal('hide');
            $(".modal-backdrop").remove();
        }

        if(end - start <= 0){
          $("#delModal").modal('show');
          $("#alert-centent").html('Set time incorect!');
          $("#alert-head").html('Warning!');
          $(".del-event").css('display', 'none');
          window.setTimeout(hide_alertModal, 2000);
        }else{
          //-- Check Booked
          var eventArr = new Array();
          var endeventArr = new Array();
          var current_url = document.URL;
          var current_urlArr = current_url.split('/');
          var url = '' + current_urlArr[0] + '//'+current_urlArr[2] + '/'+current_urlArr[3];
            $.get(url + '/'+ room +'-room'+ '/' + dateConvert, function(data){
                $.each(data, function(index, value) {
                    var names = value.event;
                    var nameArr = names.split(',');
                    $.each(nameArr, function(index, value) {
                        eventArr.push(value);
                    });
                });

                    var text = '';
                    var i = new Number(start);
                    while (i <= end) {
                        text += "," + i;
                        i += 0.5;
                    }
                    
                    var bookArr2 = text.split(',');
                    var endBooked = false;
                    bookArr2.splice(-1,1)
                    bookArr2.shift();

                    $.each(bookArr2, function(index, book) {
                        if(eventArr.includes(book)){
                            endBooked = true;
                        }
                    });

                    var booked = eventArr.includes(start);
                    if(booked || endBooked){
                      $("#delModal").modal('show');
                      $("#alert-centent").html('This time booked!');
                      $("#alert-head").html('Warning!');
                      $(".del-event").css('display', 'none');
                      window.setTimeout(hide_alertModal, 2000);
                    }else{
                      $.ajax({
                            type: "POST",
                            url: route,
                            data: form_data.serialize(),
                            success: function( msg ) {
                                hide_mainModal();
                                $("#delModal").modal('show');
                                $("#alert-centent").html('Booked!');
                                $("#alert-head").html('Warning!');
                                $(".del-event").css('display', 'none');
                                window.setTimeout(hide_alertModal, 2000);
                                // Chang Events
                                window.setTimeout(location.reload(true), 3000);
                            },
                            error: function(httpObj, textStatus) { 
                                if (httpObj.status==400) {
                                    window.location.href = "/login";
                                } else if (httpObj.status==500){
                                  $("#delModal").modal('show');
                                  $("#alert-centent").html('This time booked!');
                                  $("#alert-head").html('Warning!');
                                  $(".del-event").css('display', 'none');
                                  window.setTimeout(hide_alertModal, 2000);
                                }
                            }
                        });
                    }
            });
        }         
    });

    $(document).on('click','#select-event-submit',function(event){
      $('#event-select').html('');
      //-- Convert Date
      var fromArr = $("#dateFrom").val().split('-');
      var fromConvert = fromArr[2].concat('-' + fromArr[1] + '-').concat(fromArr[0]);

      var toArr = $("#dateTo").val().split('-');
      var toConvert = toArr[2].concat('-' + toArr[1] + '-').concat(toArr[0]);

      var room = $("#room").val();

      //Get url
      var current_url = document.URL;
      var current_urlArr = current_url.split('/');
      var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];
      var tour_id = '/admin/'+ room + '/' + fromConvert + '/' + toConvert;
      var dateArr = new Array;

      //Get data
      $.get(url + tour_id, function (data) {
            $('#event-load').css('display', 'none');
            $('#event-select').css('display', 'block');  
              //success data
              console.log(data);

              //Get date arr
              $.each(data.data, function(index, event) {
                if(dateArr.includes(event.date) == false){
                  dateArr.push(event.date);
                }
              }); 

              //Fill data arr
              $.each(dateArr, function(index, date) {
                var dateArr = date.split('-');
                var dateConvert = dateArr[2].concat('-' + dateArr[1] + '-').concat(dateArr[0]);
                var eventArr = new Array;
                var listEvent = '';

                $.each(data.data, function(index, event) {

                  if(date == event.date){
                    var createdArr = event.updated_at.split('-');
                    var createdConvert = dateArr[2].concat('-' + dateArr[1] + '-').concat(dateArr[0]);
                    var startConvert = "";
                    var endConvert = "";
                    var str = '';
                    str += event.start;
                    var n = str.indexOf(".");
                    var str2 = '';
                    str2 += event.end;
                    var n2 = str2.indexOf(".");

                    // Method Very Hearded *****************************
                    jQuery.extend({
                        getValues: function(url) {
                            var result = null;
                            $.ajax({
                                url: url,
                                type: 'get',
                                async: false,
                                success: function(data) {
                                    result = data.name;
                                }
                            });
                          return result;
                        }
                    });

                    // End Method Very Hearded *****************************

                    var current_url = document.URL;
                    var current_urlArr = current_url.split('/');
                    var url2 = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];
                    var tour_id2 = '/admin/user/'+ event.user_id;

                    if(n > 0){
                      startConvert += event.start;
                      startConvert = startConvert.slice(0, startConvert.length - 2);
                      startConvert += ":30";
                    }else{
                      startConvert = event.start +":00"
                    }

                    if(n2 > 0){
                      endConvert += event.end;
                      endConvert = endConvert.slice(0, endConvert.length - 2);
                      endConvert += ":30";
                    }else{
                      endConvert = event.end +":00"
                    }

                    if(event.user_id == 1){
                      listEvent += '<tr><td class="bg">'+ startConvert +'-'+ endConvert +'</td>'+
                      '<td>'+ event.subject +'</td><td id="username">admin</td><td>'+ createdConvert +'</td><td class="manage">'+
                      '<i id="'+ event.id +'" class="del-alog fas fa-trash fa-xs"></i></td></tr>';
                    }else{
                      listEvent += '<tr><td class="bg">'+ startConvert +'-'+ endConvert +'</td>'+
                      '<td>'+ event.subject +'</td><td id="username">'+ $.getValues(url2 + tour_id2) +'</td><td>'+ createdConvert +'</td><td class="manage">'+
                      '<i id="'+ event.id +'" class="del-alog fas fa-trash fa-xs"></i></td></tr>';
                    }
                  }
                });

                $('#event-select').append(
                  '<li><div class="event"><div class="header"><p><i class="far fa-calendar-alt fa-xs"></i>'+
                  ' '+ dateConvert +'</p><i title="Add Event" id="'+ dateConvert +'" class="add-event far fa-calendar-plus fa-lg"></i>'+
                  '</div><div class="content"><table><thead><tr class="header1"><th class="bg"><i class="far fa-clock"></i> Time</th>'+
                  '<th><i class="far fa-edit"></i> Subject</th><th><i class="far fa-user"></i> User</th><th><i class="fas fa-pen"></i> Created</th>'+
                  '<th></th></tr></thead><tbody id="list-event-select">'+ 
                    listEvent +
                  '</tbody></table></div></div></li>'
                  );
              }); 

              // $('.pagiante-list-select').append('{{ '+  +' }}');
      });
    });
</script>  
@endsection  