@extends('admin/layout')
@section('title', 'Admin')
@section('css')
    <link rel="stylesheet" href="{{URL::asset('css/admin.css')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection 
    @section('content')
                <div class="row" style="margin-right: 5px">
                    <div class="rooms col col-sm-12 col-lg-6">
                        <a href="{{route("admin-sroom")}}"> S-Room <span>List Events</span></a>
                        <div class="dummy"></div>
                        <table class="text-center">
                            <thead>
                                <tr>
                                    <th><i class="far fa-clock"></i> Time</th>
                                    <th style="width: 60%"><i class="far fa-edit"></i> Subject</th>
                                    <th><i class="far fa-user"></i> User</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sroomData as $item)
                                    <tr>
                                        <td>{{$item->start}}:00 > {{$item->end}}:00</td>
                                        <td style="border-left: rgb(82, 82, 82) 1px solid; border-right: rgb(82, 82, 82) 1px solid;">{{$item->subject}}</td>
                                        <td>
                                            @if($item->user_id == 1)
                                                Admin
                                            @else
                                                @foreach ($users as $user)
                                                    @if($user->id == $item->user_id)
                                                        {{$user->name}}
                                                    @endif 
                                                @endforeach
                                            @endif    
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="rooml col-sm-12 col-lg-6">
                        <a href="{{route("admin-lroom")}}"> L-Room <span>List Events</span></a>
                        <div class="dummy"></div>
                        <table class="text-center" style="width: 100%;" >
                            <thead>
                                <tr>
                                    <th><i class="far fa-clock"></i> Time</th>
                                    <th style="width: 60%"><i class="far fa-edit"></i> Subject</th>
                                    <th><i class="far fa-user"></i> User</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lroomData as $item)
                                    <tr>
                                        <td>{{$item->start}}:00 > {{$item->end}}:00</td>
                                        <td style="border-left: rgb(82, 82, 82) 1px solid; border-right: rgb(82, 82, 82) 1px solid;">{{$item->subject}}</td>
                                        <td>
                                            @if($item->user_id == 1)
                                                Admin
                                            @else
                                                @foreach ($users as $user)
                                                    @if($user->id == $item->user_id)
                                                        {{$user->name}}
                                                    @endif 
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row"  style="margin-right: 5px; padding-right: 0px;">
                    <div class="users col col-sm-12" style="padding-right: 0px;">
                        <a href="{{route("admin-users")}}"><p class="head">Users <span>List</span></p></a>
                        <div class=" list">
                            <ul>
                                @foreach ($users as $item)
                                    <li>
                                        <div class="user">
                                            <div class="email">
                                                <span>#E-mail</span>
                                                <i class="fas fa-at"></i>
                                                <p>{{$item->email}}</p>
                                            </div>
                                            <div class="username">
                                                <span>#Username</span>
                                                <i class="fas fa-user"></i>
                                                <p>{{$item->name}}</p>
                                            </div>
                                            <div class="time">
                                                <span>#Register</span>
                                                <i class="far fa-clock"></i> 
                                                <p>{{$item->created_at}}</p>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
    @endsection  
