@extends('admin/layout')
@section('title', 'Admin Users')
@section('css')
    <link rel="stylesheet" href="{{URL::asset('css/user.css')}}">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="{{URL::asset('js/bootstrap.js')}}"></script>
@endsection
@section('content') 
    <!-- Modal Add-->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form id="add-user" action="{{route('postUser')}}" method="POST">
              @csrf
              
              <input type="hidden" name="id" id="id"/>
              <div class="card text-dark bg-white">
              <div class="card-header" >
                <h5 class="card-title">
                  <span class="card-title-span">Add User</span>
                </h5>
              </div>
              <div class="card-body">
                <ul>
                  <li>
                      E-mail
                      <input class="email" id="email" name="email" type="email" required>
                  </li>
                  <li>
                      Username
                      <input class="username" id="username" name="username" type="text" required>
                  </li>
                  <li>
                      Password
                      <input class="password" id="password" name="password" type="password" required>
                  </li>
                  <li>
                      Confirm Password
                      <input class="password2" id="password2" name="password2" type="password" required>
                  </li>
                </ul>
              </div>
              <div class="card-footer bg-transparent text-center">
                <button class="btn" id="btn" type="submit"><span>Add User</span></button>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Delete-->
     <div class="modal fade modal-del" id="delModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="top: 150px;">
          <div class="modal-content">
              <div class="card text-dark bg-white">
              <div class="card-header" ><h5 class="card-title"><i class="fas fa-user"> </i> <span>Puttipong</span></h5></div>
              <div class="card-body text-center">
                Are You Sure?
              </div>
              <div class="card-footer bg-transparent text-center"><a><button class="btn del-event"><span>Delete User</span></button></a></div>
              </div>
          </div>
        </div>
      </div>

      <div class="users col col-sm-12">
        <p class="head">
            <i class="fas fa-user"></i>
            <i class="add-far fas fa-user-plus fa-lg"  data-toggle="modal" data-target="#addModal"></i> 
            Users 
            <span>List</span>
        </p>
        <div class="search" style="display: none">
            <i class="fas fa-search"></i>
            <input type="text" name="search" id="search">
            <button>ค้นหา</button>
        </div>
        <div class=" list">
            <ul>
                @foreach ($users as $item)
                <li>
                    <div class="user">
                        <div class="email">
                            <span>#E-mail</span>
                            <i class="fas fa-at"></i>
                            <p>{{$item->email}}</p>
                        </div>
                        <div class="username">
                            <span>#Username</span>
                            <i class="fas fa-user"></i>
                            <p>{{$item->name}}</p>
                        </div>
                        <div class="time">
                            <span>#Register</span>
                            <i class="far fa-clock"></i> 
                            <p>{{date('d/m/Y : H:m', strtotime($item->created_at))}}</p>
                        </div>
                        <div class="manage">
                            @if($item->email != 'admin@web')
                              <i id="{{$item->id}}" class="del-alog fas fa-trash"></i>
                            @endif
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            {{ $users->links() }}    
        </div>
      </div>

      <script type="text/javascript">
       $(document).on('click','.edit-user',function(event){
             var url = "/admin/user";
             var tour_id= $(this).attr('id');
             $('#addModal').modal('show');
             $.get(url + '/' + tour_id, function (data) {
                 //success data
                 console.log(data);
                 $('#username').val(data.name);
                 $('#email').val(data.email);
                 $('#id').val(data.id);
                 $('#password').attr("placeholder", "New Password");
                 $('#btn').html("Update User");
                 $('.card-title-span').html("Update User");
                 $('#add-user').attr('action', "edit-user");
                 $('#addModal').modal('show');
             }) ;
           }); 
           
        $(document).on('click','.del-event',function(event){
            var current_url = document.URL;
            var current_urlArr = current_url.split('/');
            var url = ''+current_urlArr[0]+'//'+current_urlArr[2]+'/'+current_urlArr[3];
            var urlConvert = url + '/del/user/';
            var tour_id= $(this).attr('id');
            $.get(urlConvert + tour_id + '/1') ;
              window.setTimeout(location.reload(true), 1500);
           });  
   
       $(document).on('click','.del-alog',function(event){
           $('#delModal').modal('show');
           var tour_id= $(this).attr('id');
           $('.del-event').attr("id",tour_id);
           }); 
   </script>  
@endsection  