<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{URL::asset('css/nav.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Dosis|Poppins&display=swap" rel="stylesheet">
    @yield('css')
</head>
<body>
@if(Auth::user()->email == 'admin@web')
    <div class="container-fluid">
        <div class="row">
            <div class="nav-mobile col col-sm-12" >
                <nav class="navbar navbar-dark " style="background-color: rgb(1, 168, 146);">
                <a class="navbar-brand" href="{{route('admin-home')}}">Admin</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 text-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('admin-home')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route("admin-sroom")}}">S - Room</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route("admin-lroom")}}">L - Room</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route("admin-users")}}">Users</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
            </div>
        </div>
        <div class="row">
            <div class="slid col col-sm-2">
                <span ><a href="{{route("admin-home")}}">Meeting Booking Room</a></span>
                <ul>
                    <li><a href="{{route("admin-home")}}" style="margin-left: -13px;"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="{{route("admin-sroom")}}" style="margin-left: 7px;"><i class="fas fa-users"></i> S - Room</a></li>
                    <li><a href="{{route("admin-lroom")}}" style="margin-left: 7px;"><i class="fas fa-users"></i> L - Room</a></li>
                    <li><a href="{{route("admin-users")}}" style="margin-left: -10px;"><i class="fas fa-user"></i> Users</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt" title="Logout"></i> {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>    
                    </li>
                </ul>
            </div>
            <div class="content col col-sm-10">
                @yield('content')
            </div>
        </div>  
    </div>
    @else
        <script type="text/javascript">
            window.location = "/";//here double curly bracket
        </script>           
    @endif
</body>
</html>