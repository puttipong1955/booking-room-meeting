<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckData;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// --- Public ---
// Get
Route::get('/', function () {
    return view('index');
})->name('index');
Route::get('/s-room', 'CarlendarController@sroomData')->name('sroom');
Route::get('/s-room/{date}', 'CarlendarController@sroomDate');
Route::get('/l-room', 'CarlendarController@lroomData')->name('lroom');
Route::get('/l-room/{date}', 'CarlendarController@lroomDate');
Route::get('/event/{id}', 'CarlendarController@getEvent');
Route::get('/user/{id}', 'CarlendarController@getUser');
Route::get('/test', function () {
    return view('test');
});
// Add
Route::post('/submit-event', 'CarlendarController@addEvent')->name('postData2')->middleware('auth');
Route::post('/admin/submit-event', 'CarlendarController@addEvent');
Route::post('/submit-user', 'AdminController@register')->name('postUser');
Route::post('/admin/submit-user', 'AdminController@register');
// Edit
Route::post('/edit-event', 'CarlendarController@editEvent');
Route::post('/admin/edit-event', 'CarlendarController@editEvent');
// Delete
Route::get('/del-event/{room}/{id}/{admin}', 'CarlendarController@delEvent');

// --- Private ---
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Get
Route::get('/admin', 'AdminController@index')->name('admin-home');
Route::get('/admin/s-room', 'AdminController@getSroom')->name('admin-sroom');
Route::get('/admin/l-room', 'AdminController@getLroom')->name('admin-lroom');
Route::get('/admin/users', 'AdminController@getUsers')->name('admin-users');
Route::get('/admin/user/{id}', 'AdminController@getUser')->name('admin-user');
Route::get('/admin/{room}/{from}/{to}', 'AdminController@getEventBetween');
// Edit
Route::post('/admin/edit-user', 'AdminController@editUser')->name('editUser');
// Delete
Route::delete('/admin/del-user/{id}/{admin}', 'AdminController@delUser')->name('delUser');
Route::get('/del/user/{id}/{admin}', 'AdminController@delUser')->name('delUser');
